<?php

  class Consumo extends CI_Model
  {

    function __construct()
    {
      parent::__construct();
    }

    function insertar($datosNewConsumo){
        return $this->db->insert("consumo",$datosNewConsumo);
    }

    public function obtenerConsumos()
      {
        $listadoConsumos=$this->db->get("consumo");
        if ($listadoConsumos->num_rows()>0) {
          return $listadoConsumos->result();
         } else {
            return FALSE;
         }
      }

  function borrar($id_consumo){
    $this->db->where('id_consumo',$id_consumo);
    return $this->db->delete('consumo');
  }

  function obtenerID($id_consumo)
   {
     $this->db->where('id_consumo',$id_consumo);
     $Consumo=$this->db->get('consumo');
     if ($Consumo->num_rows()>0) {
       return $Consumo->row();
     } else {
       return false;
     }
   }
  function obtenerUnico($id_consumo)
  {
    $this->db->where('id_consumo',$id_consumo);
    $Consumo=$this->db->get('consumo');
    if ($Consumo->num_rows()>0) {
      return $Consumo->row();
    } else {
      return false;
    }
  }

  function actualizar($id_consumo,$datosEditados)
  {
    $this->db->where('id_consumo',$id_consumo);
    return $this->db->update('consumo',$datosEditados);
  }
  }//close the clas

 ?>

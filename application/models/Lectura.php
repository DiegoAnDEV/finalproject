<?php
  /**
   *
   */
  class Lectura extends CI_Model
  {

    function __construct()
    {
      parent::__construct();
    }

    function insertar($datosNewLectura){
        return $this->db->insert("lectura",$datosNewLectura);
    }

    public function obtenerLecturas()
      {
        $listadoLecturas=$this->db->get("lectura");
        if ($listadoLecturas->num_rows()>0) {
          return $listadoLecturas->result();
         } else {
            return FALSE;
         }
      }
      public function obtenerConsumos()
        {
          $listadoConsumos=$this->db->get("consumo");
          if ($listadoConsumos->num_rows()>0) {
            return $listadoConsumos->result();
           } else {
              return FALSE;
           }
        }

        public function obtenerHistoriales()
          {
            $listadoLecturas=$this->db->get("historial_propietario");
            if ($listadoLecturas->num_rows()>0) {
              return $listadoLecturas->result();
             } else {
                return FALSE;
             }
          }
  function borrar($id_lec){
    $this->db->where('id_lec',$id_lec);
    return $this->db->delete('lectura');
  }


  function obtenerID($id_lec)
   {
     $this->db->where('id_lec',$id_lec);
     $Lectura=$this->db->get('lectura');
     if ($Lectura->num_rows()>0) {
       return $Lectura->row();
     } else {
       return false;
     }
   }
  function obtenerUnico($id_lec)
  {
    $this->db->where('id_lec',$id_lec);
    $Lectura=$this->db->get('lectura');
    if ($Lectura->num_rows()>0) {
      return $Lectura->row();
    } else {
      return false;
    }
  }

  function actualizar($id_lec,$datosEditados)
  {
    $this->db->where('id_lec',$id_lec);
    return $this->db->update('lectura',$datosEditados);
  }
  }//close the clas

 ?>

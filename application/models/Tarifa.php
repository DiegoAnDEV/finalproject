<?php
  class Tarifa extends CI_Model
  {
    function __construct()
    {
      parent::__construct();
    }

    public function getAll()
    {
      $result=$this->db->get("tarifa");
      if ($result->num_rows()>0) {
        return $result->result();
      } else {
        return false;
      }
    }

    function insertar($datosNewTarifa){
      return $this->db->insert("tarifa",$datosNewTarifa);
    }

    function borrar($id_tar){
    $this->db->where('id_tar',$id_tar);
    return $this->db->delete('tarifa');
    }

    function obtenerID($id_tar)
    {
      $this->db->where('id_tar',$id_tar);
      $Tarifa=$this->db->get('tarifa');
      if ($Tarifa->num_rows()>0) {
        return $Tarifa->row();
      } else {
        return false;
      }
    }

    function actualizar($id_tar,$datosEditados)
    {
      $this->db->where('id_tar',$id_tar);
      return $this->db->update('tarifa',$datosEditados);
    }



  }
?>

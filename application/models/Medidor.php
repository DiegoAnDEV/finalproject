<?php
  class Medidor extends CI_Model
  {
    function __construct()
    {
      parent::__construct();
    }
    public function getMedidor()
      {
        $result=$this->db->get("ruta");
        if ($result->num_rows()>0) {
          return $result->result();
        } else {
          return false;
        }
      }
      public function getTarifa()
        {
          $result=$this->db->get("tarifa");
          if ($result->num_rows()>0) {
            return $result->result();
          } else {
            return false;
          }
        }

    public function getAll()
    {
      $result=$this->db->get("medidor");
      if ($result->num_rows()>0) {
        return $result->result();
      } else {
        return false;
      }
    }

    function insertar($datosNewMedidor){
      return $this->db->insert("medidor",$datosNewMedidor);
    }

    function borrar($id_med){
    $this->db->where('id_med',$id_med);
    return $this->db->delete('medidor');
    }

    function obtenerID($id_med)
    {
      $this->db->where('id_med',$id_med);
      $Ruta=$this->db->get('medidor');
      if ($Ruta->num_rows()>0) {
        return $Ruta->row();
      } else {
        return false;
      }
    }

    function actualizar($id_med,$datosEditados)
    {
      $this->db->where('id_med',$id_med);
      return $this->db->update('medidor',$datosEditados);
    }



  }
?>

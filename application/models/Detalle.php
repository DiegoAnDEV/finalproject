<?php
  /**
   *
   */
  class Detalle extends CI_Model
  {

    function __construct()
    {
      parent::__construct();
    }

    function insertar($datosNewDetalle){
        return $this->db->insert("detalle",$datosNewDetalle);
    }

    public function obtenerDetalles()
      {
        $listadoDetalles=$this->db->get("detalle");
        if ($listadoDetalles->num_rows()>0) {
          return $listadoDetalles->result();
         } else {
            return FALSE;
         }
      }

      public function obtenerRecaudaciones()
        {
          $listadoRecaudaciones=$this->db->get("recaudacion");
          if ($listadoRecaudaciones->num_rows()>0) {
            return $listadoRecaudaciones->result();
           } else {
              return FALSE;
           }
        }

        public function obtenerLecturas()
          {
            $listadoLecturas=$this->db->get("lectura");
            if ($listadoLecturas->num_rows()>0) {
              return $listadoLecturas->result();
             } else {
                return FALSE;
             }
          }
  function borrar($id_det){
    $this->db->where('id_det',$id_det);
    return $this->db->delete('detalle');
  }


  function obtenerID($id_det)
   {
     $this->db->where('id_det',$id_det);
     $Detalle=$this->db->get('detalle');
     if ($Detalle->num_rows()>0) {
       return $Detalle->row();
     } else {
       return false;
     }
   }
  function obtenerUnico($id_det)
  {
    $this->db->where('id_det',$id_det);
    $Detalle=$this->db->get('detalle');
    if ($Detalle->num_rows()>0) {
      return $Detalle->row();
    } else {
      return false;
    }
  }

  function actualizar($id_det,$datosEditados)
  {
    $this->db->where('id_det',$id_det);
    return $this->db->update('detalle',$datosEditados);
  }
  }//close the clas

 ?>

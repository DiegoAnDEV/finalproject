<?php
  class Usuario extends CI_Model
  {
    function __construct()
    {
      parent::__construct();
    }

    public function getAll()
    {
      $result=$this->db->get("usuario");
      if ($result->num_rows()>0) {
        return $result->result();
      } else {
        return false;
      }
    }

    function insertar($datosNewProduct){
      return $this->db->insert("usuario",$datosNewProduct);
    }

    function borrar($id_usu){
      $this->db->where('id_usu',$id_usu);
      return $this->db->delete('usuario');
    }

    function obtenerID($id_usu)
    {
      $this->db->where('id_usu',$id_usu);
      $Usuario=$this->db->get('usuario');
      if ($Usuario->num_rows()>0) {
        return $Usuario->row();
      } else {
        return false;
      }
    }

    function actualizar($id_usu,$datosEditados)
    {
      $this->db->where('id_usu',$id_usu);
      return $this->db->update('usuario',$datosEditados);
    }

  }
?>

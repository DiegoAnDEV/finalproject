<?php
  class Historial extends CI_Model
  {
    function __construct()
    {
      parent::__construct();
    }
    public function getTipos()
      {
        $result=$this->db->get("medidor");
        if ($result->num_rows()>0) {
          return $result->result();
        } else {
          return false;
        }
      }
      public function getSocio()
        {
          $result=$this->db->get("socio");
          if ($result->num_rows()>0) {
            return $result->result();
          } else {
            return false;
          }
        }

    public function getAll()
    {
      $result=$this->db->get("historial_propietario");
      if ($result->num_rows()>0) {
        return $result->result();
      } else {
        return false;
      }
    }

    function insertar($datosNewHistorial){
      return $this->db->insert("historial_propietario",$datosNewHistorial);
    }

    function borrar($id_his){
    $this->db->where('id_his',$id_his);
    return $this->db->delete('historial_propietario');
    }

    function obtenerID($id_his)
    {
      $this->db->where('id_his',$id_his);
      $Historial=$this->db->get('historial_propietario');
      if ($Historial->num_rows()>0) {
        return $Historial->row();
      } else {
        return false;
      }
    }

    function actualizar($id_his,$datosEditados)
    {
      $this->db->where('id_his',$id_his);
      return $this->db->update('historial_propietario',$datosEditados);
    }




  }
?>

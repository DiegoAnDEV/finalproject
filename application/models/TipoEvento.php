<?php

  class TipoEvento extends CI_Model
  {

    function __construct()
    {
      parent::__construct();
    }

    function insertar($datosNewTipoEvento){
        return $this->db->insert("tipo_evento",$datosNewTipoEvento);
    }

    public function obtenerTipoEventos()
      {
        $listadoTipoEventos=$this->db->get("tipo_evento");
        if ($listadoTipoEventos->num_rows()>0) {
          return $listadoTipoEventos->result();
         } else {
            return FALSE;
         }
      }

  function borrar($id_te){
    $this->db->where('id_te',$id_te);
    return $this->db->delete('tipo_evento');
  }

  function obtenerID($id_te)
   {
     $this->db->where('id_te',$id_te);
     $TipoEvento=$this->db->get('tipo_evento');
     if ($TipoEvento->num_rows()>0) {
       return $TipoEvento->row();
     } else {
       return false;
     }
   }
  function obtenerUnico($id_te)
  {
    $this->db->where('id_te',$id_te);
    $TipoEvento=$this->db->get('tipo_evento');
    if ($TipoEvento->num_rows()>0) {
      return $TipoEvento->row();
    } else {
      return false;
    }
  }

  function actualizar($id_te,$datosEditados)
  {
    $this->db->where('id_te',$id_te);
    return $this->db->update('tipo_evento',$datosEditados);
  }
  }//close the clas

 ?>

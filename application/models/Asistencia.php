<?php
  /**
   *
   */
  class Asistencia extends CI_Model
  {

    function __construct()
    {
      parent::__construct();
    }

    function insertar($datosNewAsistencia){
        return $this->db->insert("asistencia",$datosNewAsistencia);
    }

    public function obtenerAsistencias()
      {
        $listadoAsistencias=$this->db->get("asistencia");
        if ($listadoAsistencias->num_rows()>0) {
          return $listadoAsistencias->result();
         } else {
            return FALSE;
         }
      }

      public function obtenerEventos()
        {
          $listadoEventos=$this->db->get("evento");
          if ($listadoEventos->num_rows()>0) {
            return $listadoEventos->result();
           } else {
              return FALSE;
           }
        }

        public function obtenerSocios()
          {
            $listadoSocios=$this->db->get("socio");
            if ($listadoSocios->num_rows()>0) {
              return $listadoSocios->result();
             } else {
                return FALSE;
             }
          }
  function borrar($id_asi){
    $this->db->where('id_asi',$id_asi);
    return $this->db->delete('asistencia');
  }


  function obtenerID($id_asi)
   {
     $this->db->where('id_asi',$id_asi);
     $Asistencia=$this->db->get('asistencia');
     if ($Asistencia->num_rows()>0) {
       return $Asistencia->row();
     } else {
       return false;
     }
   }
  function obtenerUnico($id_asi)
  {
    $this->db->where('id_asi',$id_asi);
    $Asistencia=$this->db->get('asistencia');
    if ($Asistencia->num_rows()>0) {
      return $Asistencia->row();
    } else {
      return false;
    }
  }

  function actualizar($id_asi,$datosEditados)
  {
    $this->db->where('id_asi',$id_asi);
    return $this->db->update('asistencia',$datosEditados);
  }
  }//close the clas

 ?>

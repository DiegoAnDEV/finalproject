<?php
  class Comunicado extends CI_Model
  {
    function __construct()
    {
      parent::__construct();
    }

    public function getAll()
    {
      $result=$this->db->get("comunicado");
      if ($result->num_rows()>0) {
        return $result->result();
      } else {
        return false;
      }
    }

    function insertar($datosNewComunicado){
      return $this->db->insert("comunicado",$datosNewComunicado);
    }

    function borrar($id_com){
    $this->db->where('id_com',$id_com);
    return $this->db->delete('comunicado');
    }

    function obtenerID($id_com)
    {
      $this->db->where('id_com',$id_com);
      $Comunicado=$this->db->get('comunicado');
      if ($Comunicado->num_rows()>0) {
        return $Comunicado->row();
      } else {
        return false;
      }
    }

    function actualizar($id_com,$datosEditados)
    {
      $this->db->where('id_com',$id_com);
      return $this->db->update('comunicado',$datosEditados);
    }



  }
?>

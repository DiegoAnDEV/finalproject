<?php
  class Exedente extends CI_Model
  {
    function __construct()
    {
      parent::__construct();
    }

    public function getAll()
    {
      $result=$this->db->get("excedente");
      if ($result->num_rows()>0) {
        return $result->result();
      } else {
        return false;
      }
    }

    function insertar($datosNewExedente){
      return $this->db->insert("excedente",$datosNewExedente);
    }

    function borrar($id_ex){
    $this->db->where('id_ex',$id_ex);
    return $this->db->delete('excedente');
    }

    function obtenerID($id_ex)
    {
      $this->db->where('id_ex',$id_ex);
      $Exedente=$this->db->get('excedente');
      if ($Exedente->num_rows()>0) {
        return $Exedente->row();
      } else {
        return false;
      }
    }

    function actualizar($id_ex,$datosEditados)
    {
      $this->db->where('id_ex',$id_ex);
      return $this->db->update('excedente',$datosEditados);
    }



  }
?>

<?php
  class Configuracion extends CI_Model
  {
    function __construct()
    {
      parent::__construct();
    }

    public function getAll()
    {
      $result=$this->db->get("configuracion");
      if ($result->num_rows()>0) {
        return $result->result();
      } else {
        return false;
      }
    }

    function insertar($datosNewProduct){
      return $this->db->insert("configuracion",$datosNewProduct);
    }

    function borrar($id_con){
    $this->db->where('id_con',$id_con);
    return $this->db->delete('configuracion');
    }

    function obtenerID($id_con)
    {
      $this->db->where('id_con',$id_con);
      $Configuracion=$this->db->get('configuracion');
      if ($Configuracion->num_rows()>0) {
        return $Configuracion->row();
      } else {
        return false;
      }
    }

    function actualizar($id_con,$datosEditados)
    {
      $this->db->where('id_con',$id_con);
      return $this->db->update('configuracion',$datosEditados);
    }



  }
?>

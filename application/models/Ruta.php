<?php
  class Ruta extends CI_Model
  {
    function __construct()
    {
      parent::__construct();
    }

    public function getAll()
    {
      $result=$this->db->get("ruta");
      if ($result->num_rows()>0) {
        return $result->result();
      } else {
        return false;
      }
    }

    function insertar($datosNewRuta){
      return $this->db->insert("ruta",$datosNewRuta);
    }

    function borrar($id_rut){
    $this->db->where('id_rut',$id_rut);
    return $this->db->delete('ruta');
    }

    function obtenerID($id_rut)
    {
      $this->db->where('id_rut',$id_rut);
      $Ruta=$this->db->get('ruta');
      if ($Ruta->num_rows()>0) {
        return $Ruta->row();
      } else {
        return false;
      }
    }

    function actualizar($id_rut,$datosEditados)
    {
      $this->db->where('id_rut',$id_rut);
      return $this->db->update('ruta',$datosEditados);
    }



  }
?>

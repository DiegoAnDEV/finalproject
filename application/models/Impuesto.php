<?php
  class Impuesto extends CI_Model
  {
    function __construct()
    {
      parent::__construct();
    }

    public function getAll()
    {
      $result=$this->db->get("impuesto");
      if ($result->num_rows()>0) {
        return $result->result();
      } else {
        return false;
      }
    }

    function insertar($datosNewProduct){
      return $this->db->insert("impuesto",$datosNewProduct);
    }

    public function getByCity()
    {
      $sql="select city_cin, COUNT(*) as total_city, (COUNT(*)*100.0/(SELECT COUNT(*) FROM cinema)) as percentage from cinema GROUP by city_cin;";
      $result=$this->db->query($sql);
      if ($result->num_rows()>0) {
        // Row es para un solo resultado
        // return $result->row()->totalMoney;
        return $result->result();
      } else {
        return 0;
      }
    }

    function borrar($id_imp){
    $this->db->where('id_imp',$id_imp);
    return $this->db->delete('impuesto');
    }

    function obtenerID($id_imp)
    {
      $this->db->where('id_imp',$id_imp);
      $Impuesto=$this->db->get('impuesto');
      if ($Impuesto->num_rows()>0) {
        return $Impuesto->row();
      } else {
        return false;
      }
    }

    function actualizar($id_imp,$datosEditados)
    {
      $this->db->where('id_imp',$id_imp);
      return $this->db->update('impuesto',$datosEditados);
    }



  }
?>

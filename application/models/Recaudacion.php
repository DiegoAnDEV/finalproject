<?php
  class Recaudacion extends CI_Model
  {
    function __construct()
    {
      parent::__construct();
    }

    public function getAll()
    {
      $result=$this->db->get("recaudacion");
      if ($result->num_rows()>0) {
        return $result->result();
      } else {
        return false;
      }
    }

    public function getSocio()
    {
      $result=$this->db->get("socio");
      if ($result->num_rows()>0) {
        return $result->result();
      } else {
        return false;
      }
    }

    function insertar($datosNewProduct){
      return $this->db->insert("recaudacion",$datosNewProduct);
    }

    function borrar($id_rec){
    $this->db->where('id_rec',$id_rec);
    return $this->db->delete('recaudacion');
    }

    function obtenerID($id_rec)
    {
      $this->db->where('id_rec',$id_rec);
      $Recaudacion=$this->db->get('recaudacion');
      if ($Recaudacion->num_rows()>0) {
        return $Recaudacion->row();
      } else {
        return false;
      }
    }

    function actualizar($id_rec,$datosEditados)
    {
      $this->db->where('id_rec',$id_rec);
      return $this->db->update('recaudacion',$datosEditados);
    }



  }
?>

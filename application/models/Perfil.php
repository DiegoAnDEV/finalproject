<?php

  class Perfil extends CI_Model
  {

    function __construct()
    {
      parent::__construct();
    }

    function insertar($datosNewPerfil){
        return $this->db->insert("perfil",$datosNewPerfil);
    }

    public function obtenerPerfiles()
      {
        $listadoPerfiles=$this->db->get("perfil");
        if ($listadoPerfiles->num_rows()>0) {
          return $listadoPerfiles->result();
         } else {
            return FALSE;
         }
      }
      
  function borrar($id_per){
    $this->db->where('id_per',$id_per);
    return $this->db->delete('perfil');
  }

  function obtenerID($id_per)
   {
     $this->db->where('id_per',$id_per);
     $Perfil=$this->db->get('perfil');
     if ($Perfil->num_rows()>0) {
       return $Perfil->row();
     } else {
       return false;
     }
   }
  function obtenerUnico($id_per)
  {
    $this->db->where('id_per',$id_per);
    $Perfil=$this->db->get('perfil');
    if ($Perfil->num_rows()>0) {
      return $Perfil->row();
    } else {
      return false;
    }
  }

  function actualizar($id_per,$datosEditados)
  {
    $this->db->where('id_per',$id_per);
    return $this->db->update('perfil',$datosEditados);
  }
  }//close the clas

 ?>

<?php
  class Evento extends CI_Model
  {
    function __construct()
    {
      parent::__construct();
    }

    public function getAll()
    {
      $result=$this->db->get("evento");
      if ($result->num_rows()>0) {
        return $result->result();
      } else {
        return false;
      }
    }

    public function getTipos()
    {
      $result=$this->db->get("tipo_evento");
      if ($result->num_rows()>0) {
        return $result->result();
      } else {
        return false;
      }
    }

    function insertar($datosNewProduct){
      return $this->db->insert("evento",$datosNewProduct);
    }

    function borrar($id_eve){
    $this->db->where('id_eve',$id_eve);
    return $this->db->delete('evento');
    }

    function obtenerID($id_eve)
    {
      $this->db->where('id_eve',$id_eve);
      $Impuesto=$this->db->get('evento');
      if ($Impuesto->num_rows()>0) {
        return $Impuesto->row();
      } else {
        return false;
      }
    }

    function actualizar($id_eve,$datosEditados)
    {
      $this->db->where('id_eve',$id_eve);
      return $this->db->update('evento',$datosEditados);
    }



  }
?>

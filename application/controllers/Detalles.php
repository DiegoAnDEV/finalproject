<?php

class Detalles extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Detalle');
    }

    public function index(){
        //print_r($data);
        $data['listDet']=$this->Detalle->obtenerDetalles();
        $this->load->view('header');
        $this->load->view('detalles/index',$data);
        $this->load->view('footer');
    }

    public function nuevo(){
        $data['lecturas']=$this->Detalle->obtenerLecturas();
        $data['recaudaciones']=$this->Detalle->obtenerRecaudaciones();
        $this->load->view('header');
        $this->load->view('detalles/nuevo',$data);
        $this->load->view('footer');
    }

    public function guardar(){
   $datosNewDetalle=array(
     'detalle_det'=>$this->input->post('detalle_det'),
     'cantidad_det'=>$this->input->post('cantidad_det'),
     'fk_id_lec'=>$this->input->post('fk_id_lec'),
     'fk_id_rec'=>$this->input->post('fk_id_rec'),
     'iva_det'=>$this->input->post('iva_det'),
     'subtotal_det'=>$this->input->post('subtotal_det'),
     'valor_unitario_det'=>$this->input->post('valor_unitario_det')
       );
     if ($this->Detalle->insertar($datosNewDetalle)) {
       redirect('detalles/index');
     } else {
       echo "Error al guardar, intente otra vez";
     }
   }

   public function eliminar($id_det){
     if ($this->Detalle->borrar($id_det)) {
       redirect('detalles/index');
      } else {
        echo "Error al eliminar";
      }
     }

   public function editar($id_det){
  $data['lecturas']=$this->Detalle->obtenerLecturas();
  $data['recaudaciones']=$this->Detalle->obtenerRecaudaciones();
   $data["detalleEditar"]=$this->Detalle->obtenerID($id_det);
   $this->load->view('header');
   $this->load->view('detalles/editar',$data);
   $this->load->view('footer');
 }

   public function procesarActualizacion(){
     $datosEditados = array(
       'detalle_det'=>$this->input->post('detalle_det'),
       'cantidad_det'=>$this->input->post('cantidad_det'),
       'fk_id_lec'=>$this->input->post('fk_id_lec'),
       'fk_id_rec'=>$this->input->post('fk_id_rec'),
       'iva_det'=>$this->input->post('iva_det'),
       'subtotal_det'=>$this->input->post('subtotal_det'),
       'valor_unitario_det'=>$this->input->post('valor_unitario_det')

     );
     $id_det=$this->input->post("id_det");
       if ($this->Detalle->actualizar($id_det,$datosEditados)){
       redirect('detalles/index');
       } else {
         echo "error " ;
       }
     }


}

<?php
  class Recaudaciones extends CI_Controller
  {
    function __construct()
    {
      parent::__construct();
      $this->load->model('Recaudacion');
    }

    public function index()
    {
      $data['cinemaList']=$this->Recaudacion->getAll();
      $this->load->view('header');
      $this->load->view('recaudacion/index',$data);
      $this->load->view('footer');
    }

    public function nuevo()
    {
      $data['socios']=$this->Recaudacion->getSocio();
      $this->load->view('header');
      $this->load->view('recaudacion/nuevo',$data);
      $this->load->view('footer');
    }

    public function guardar(){
  	$datosNewProduct=array(
      'estado_rec'=>$this->input->post('estado_rec'),
      'fk_id_soc'=>$this->input->post('fk_id_soc'),
      'fecha_emision_rec'=>$this->input->post('fecha_emision_rec'),
  	);
  		if ($this->Recaudacion->insertar($datosNewProduct)) {
          redirect('recaudaciones/index');
  		}
  	}

    public function editar($id_rec){
    $data["productEdit"]=$this->Recaudacion->obtenerID($id_rec);
    $data['socios']=$this->Recaudacion->getSocio();
    $this->load->view('header');
    $this->load->view('recaudacion/editar',$data);
    $this->load->view('footer');
    }

    public function procesarActualizacion(){
    $datosEditados = array(
      'id_rec'=>$this->input->post('id_rec'),
      'estado_rec'=>$this->input->post('estado_rec'),
      'fk_id_soc'=>$this->input->post('fk_id_soc'),
      'fecha_emision_rec'=>$this->input->post('fecha_emision_rec'),
		);
    $id_rec=$this->input->post("id_rec");
      if ($this->Recaudacion->actualizar($id_rec,$datosEditados)){
      redirect('recaudaciones/index');
		}
  }

  public function eliminar($id_rec){
    if ($this->Recaudacion->borrar($id_rec)) {
      redirect('recaudaciones/index');
    }
  }

}
?>

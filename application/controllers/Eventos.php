<?php
  class Eventos extends CI_Controller
  {
    function __construct()
    {
      parent::__construct();
      $this->load->model('Evento');
    }

    public function index()
    {
      $data['cinemaList']=$this->Evento->getAll();
      // Total and percentage cinemas by cit

      // $data['totalIVA']=$this->Product->getProductsByIVA("desc");
      // $data['totalAverage']=$this->Product->getProductsByAverage();
      $this->load->view('header');
      $this->load->view('evento/index',$data);
      $this->load->view('footer');
    }

    public function nuevo()
    {
      $data['tipos']=$this->Evento->getTipos();
      $this->load->view('header');
      $this->load->view('evento/nuevo',$data);
      $this->load->view('footer');
    }

    public function guardar(){
  	$datosNewProduct=array(
      'descripcion_eve'=>$this->input->post('descripcion_eve'),
      'fecha_hora_eve'=>$this->input->post('fecha_hora_eve'),
  		'lugar_eve'=>$this->input->post('lugar_eve'),
      'fk_id_te'=>$this->input->post('fk_id_te'),
  	);
  		if ($this->Evento->insertar($datosNewProduct)) {
          redirect('eventos/index');
  		}
  	}

    public function editar($id_eve){
    $data["productEdit"]=$this->Evento->obtenerID($id_eve);
    $data['tipos']=$this->Evento->getTipos();
    $this->load->view('header');
    $this->load->view('evento/editar',$data);
    $this->load->view('footer');
    }

    public function procesarActualizacion(){
    $datosEditados = array(
      'id_eve'=>$this->input->post('id_eve'),
      'descripcion_eve'=>$this->input->post('descripcion_eve'),
      'fecha_hora_eve'=>$this->input->post('fecha_hora_eve'),
  		'lugar_eve'=>$this->input->post('lugar_eve'),
      'fk_id_te'=>$this->input->post('fk_id_te'),
		);
    $id_eve=$this->input->post("id_eve");
      if ($this->Evento->actualizar($id_eve,$datosEditados)){
      redirect('eventos/index');
		}
  }

  public function eliminar($id_eve){
    if ($this->Evento->borrar($id_eve)) {
      redirect('eventos/index');
    }
  }

}
?>

<?php
  class Medidores extends CI_Controller
  {
    function __construct()
    {
      parent::__construct();
      $this->load->model('Medidor');
    }

    public function index()
    {
      $data['cinemaList']=$this->Medidor->getAll();
      // Total and percentage cinemas by cit

      // $data['totalIVA']=$this->Product->getProductsByIVA("desc");
      // $data['totalAverage']=$this->Product->getProductsByAverage();
      $this->load->view('header');
      $this->load->view('medidores/index',$data);
      $this->load->view('footer');
    }

    public function nuevo()
    {
      $data['medido']=$this->Medidor->getMedidor();
      $data['tarifa']=$this->Medidor->getTarifa();
      $this->load->view('header');
      $this->load->view('medidores/nuevo',$data);
      $this->load->view('footer');
    }

    public function guardar(){
  	$datosNewMedidor=array(
      'fk_id_rut'=>$this->input->post('fk_id_rut'),
      'fk_id_tar'=>$this->input->post('fk_id_tar'),
  		'numero_med'=>$this->input->post('numero_med'),
      'serie_med'=>$this->input->post('serie_med'),
      'marca_med'=>$this->input->post('marca_med'),
      'observacion_med'=>$this->input->post('observacion_med'),
      'estado_med'=>$this->input->post('estado_med'),
      'foto_med'=>$this->input->post('foto_med'),
      'creacion_med'=>$this->input->post('creacion_med'),
      'actualizacion_med'=>$this->input->post('actualizacion_med'),
      'lectura_inicial_med'=>$this->input->post('lectura_inicial_med'),
  	);
  		if ($this->Medidor->insertar($datosNewMedidor)) {
          redirect('medidores/index');
  		}
  	}

    public function editar($id_med){
    $data["medidorEdit"]=$this->Medidor->obtenerID($id_med);
    $this->load->view('header');
    $this->load->view('medidores/editar',$data);
    $this->load->view('footer');
    }

    public function procesarActualizacion(){
    $datosEditados = array(
      'id_med'=>$this->input->post('id_med'),
      'fk_id_rut'=>$this->input->post('fk_id_rut'),
      'fk_id_tar'=>$this->input->post('fk_id_tar'),
  		'numero_med'=>$this->input->post('numero_med'),
      'serie_med'=>$this->input->post('serie_med'),
      'marca_med'=>$this->input->post('marca_med'),
      'observacion_med'=>$this->input->post('observacion_med'),
      'estado_med'=>$this->input->post('estado_med'),
      'foto_med'=>$this->input->post('foto_med'),
      'creacion_med'=>$this->input->post('creacion_med'),
      'actualizacion_med'=>$this->input->post('actualizacion_med'),
      'lectura_inicial_med'=>$this->input->post('lectura_inicial_med'),
  	);
    $id_med=$this->input->post("id_med");
      if ($this->Medidor->actualizar($id_med,$datosEditados)){
      redirect('medidores/index');
		}
  }

  public function eliminar($id_med){
    if ($this->Medidor->borrar($id_med)) {
      redirect('medidores/index');
    }
  }

}
?>

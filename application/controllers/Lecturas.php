<?php

class Lecturas extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Lectura');
    }

    public function index(){
        //print_r($data);
        $data['listLec']=$this->Lectura->obtenerLecturas();
        $this->load->view('header');
        $this->load->view('lecturas/index',$data);
        $this->load->view('footer');
    }

    public function nuevo(){
      $data['historiales']=$this->Lectura->obtenerHistoriales();
      $data['consumos']=$this->Lectura->obtenerConsumos();
        $this->load->view('header');
        $this->load->view('lecturas/nuevo',$data);
        $this->load->view('footer');
    }

    public function guardar(){
   $datosNewLectura=array(
     'anio_lec'=>$this->input->post('anio_lec'),
     'estado_lec'=>$this->input->post('estado_lec'),
     'fk_id_consumo'=>$this->input->post('fk_id_consumo'),
     'fk_id_his'=>$this->input->post('fk_id_his'),
     'lectura_actual_lec'=>$this->input->post('lectura_actual_lec'),
     'lectura_anterior_lec'=>$this->input->post('lectura_anterior_lec'),
     'mes_lec'=>$this->input->post('mes_lec')
       );
     if ($this->Lectura->insertar($datosNewLectura)) {
       redirect('lecturas/index');
     } else {
       echo "Error al guardar, intente otra vez";
     }
   }

   public function eliminar($id_lec){
     if ($this->Lectura->borrar($id_lec)) {
       redirect('lecturas/index');
      } else {
        echo "Error al eliminar";
      }
     }

   public function editar($id_lec){
   $data['historiales']=$this->Lectura->obtenerHistoriales();
   $data['consumos']=$this->Lectura->obtenerConsumos();
   $data["lecturaEditar"]=$this->Lectura->obtenerID($id_lec);
   $this->load->view('header');
   $this->load->view('lecturas/editar',$data);
   $this->load->view('footer');
 }

   public function procesarActualizacion(){
     $datosEditados = array(
       'anio_lec'=>$this->input->post('anio_lec'),
       'estado_lec'=>$this->input->post('estado_lec'),
       'fk_id_consumo'=>$this->input->post('fk_id_consumo'),
       'fk_id_his'=>$this->input->post('fk_id_his'),
       'lectura_actual_lec'=>$this->input->post('lectura_actual_lec'),
       'lectura_anterior_lec'=>$this->input->post('lectura_anterior_lec'),
       'mes_lec'=>$this->input->post('mes_lec')

     );
     $id_lec=$this->input->post("id_lec");
       if ($this->Lectura->actualizar($id_lec,$datosEditados)){
       redirect('lecturas/index');
       } else {
         echo "error " ;
       }
     }


}

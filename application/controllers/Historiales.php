<?php
  class Historiales extends CI_Controller
  {
    function __construct()
    {
      parent::__construct();
      $this->load->model('Historial');
    }


    public function index()
    {
      $data['cinemaList']=$this->Historial->getAll();
      // Total and percentage cinemas by cit

      // $data['totalIVA']=$this->Product->getProductsByIVA("desc");
      // $data['totalAverage']=$this->Product->getProductsByAverage();
      $this->load->view('header');
      $this->load->view('historiales/index',$data);
      $this->load->view('footer');
    }

    public function nuevo()
    {
      $data['historia']=$this->Historial->getTipos();
      $data['socio']=$this->Historial->getSocio();
      $this->load->view('header');
      $this->load->view('historiales/nuevo',$data);
      $this->load->view('footer');
    }

    public function guardar(){
  	$datosNewHistorial=array(
      'fk_id_med'=>$this->input->post('fk_id_med'),
      'fk_id_soc'=>$this->input->post('fk_id_soc'),
  		'actualizacion_his'=>$this->input->post('actualizacion_his'),
      'estado_his'=>$this->input->post('estado_his'),
      'observacion_his'=>$this->input->post('observacion_his'),
      'fecha_cambio_his'=>$this->input->post('fecha_cambio_his'),
      'creacion_his'=>$this->input->post('creacion_his'),
      'propietario_actual_his'=>$this->input->post('propietario_actual_his'),

  	);
  		if ($this->Historial->insertar($datosNewHistorial)) {
          redirect('historiales/index');
  		}
  	}

    public function editar($id_his){
    $data["historialEdit"]=$this->Historial->obtenerID($id_his);
    $this->load->view('header');
    $this->load->view('historiales/editar',$data);
    $this->load->view('footer');
    }

    public function procesarActualizacion(){
    $datosEditados = array(
      'id_his'=>$this->input->post('id_his'),
      'fk_id_med'=>$this->input->post('fk_id_med'),
      'fk_id_soc'=>$this->input->post('fk_id_soc'),
  		'actualizacion_his'=>$this->input->post('actualizacion_his'),
      'estado_his'=>$this->input->post('estado_his'),
      'observacion_his'=>$this->input->post('observacion_his'),
      'fecha_cambio_his'=>$this->input->post('fecha_cambio_his'),
      'creacion_his'=>$this->input->post('creacion_his'),
      'propietario_actual_his'=>$this->input->post('propietario_actual_his'),
		);
    $id_his=$this->input->post("id_his");
      if ($this->Historial->actualizar($id_his,$datosEditados)){
      redirect('historiales/index');
		}
  }

  public function eliminar($id_his){
    if ($this->Historial->borrar($id_his)) {
      redirect('historiales/index');
    }
  }



}
?>

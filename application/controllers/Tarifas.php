<?php
  class Tarifas extends CI_Controller
  {
    function __construct()
    {
      parent::__construct();
      $this->load->model('Tarifa');
    }

    public function index()
    {
      $data['cinemaList']=$this->Tarifa->getAll();
      // Total and percentage cinemas by cit

      // $data['totalIVA']=$this->Product->getProductsByIVA("desc");
      // $data['totalAverage']=$this->Product->getProductsByAverage();
      $this->load->view('header');
      $this->load->view('tarifas/index',$data);
      $this->load->view('footer');
    }

    public function nuevo()
    {
      $this->load->view('header');
      $this->load->view('tarifas/nuevo');
      $this->load->view('footer');
    }

    public function guardar(){
  	$datosNewTarifa=array(
      'nombre_tar'=>$this->input->post('nombre_tar'),
      'descripcion_tar'=>$this->input->post('descripcion_tar'),
  		'estado_tar'=>$this->input->post('estado_tar'),
      'm3_maximo_tar'=>$this->input->post('m3_maximo_tar'),
      'tarifa_basica_tar'=>$this->input->post('tarifa_basica_tar'),
      'tarifa_excedente_tar'=>$this->input->post('tarifa_excedente_tar'),
      'valor_mora_tar'=>$this->input->post('valor_mora_tar'),
  	);
  		if ($this->Tarifa->insertar($datosNewTarifa)) {
          redirect('tarifas/index');
  		}
  	}

    public function editar($id_tar){
    $data["tarifaEdit"]=$this->Tarifa->obtenerID($id_tar);
    $this->load->view('header');
    $this->load->view('tarifas/editar',$data);
    $this->load->view('footer');
    }

    public function procesarActualizacion(){
    $datosEditados = array(
      'id_tar'=>$this->input->post('id_tar'),
      'nombre_tar'=>$this->input->post('nombre_tar'),
      'descripcion_tar'=>$this->input->post('descripcion_tar'),
  		'estado_tar'=>$this->input->post('estado_tar'),
      'm3_maximo_tar'=>$this->input->post('m3_maximo_tar'),
      'tarifa_basica_tar'=>$this->input->post('tarifa_basica_tar'),
      'tarifa_excedente_tar'=>$this->input->post('tarifa_excedente_tar'),
      'valor_mora_tar'=>$this->input->post('valor_mora_tar'),
		);
    $id_tar=$this->input->post("id_tar");
      if ($this->Tarifa->actualizar($id_tar,$datosEditados)){
      redirect('tarifas/index');
		}
  }

  public function eliminar($id_tar){
    if ($this->Tarifa->borrar($id_tar)) {
      redirect('tarifas/index');
    }
  }

}
?>

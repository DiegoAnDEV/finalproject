<?php
  class Impuestos extends CI_Controller
  {
    function __construct()
    {
      parent::__construct();
      $this->load->model('Impuesto');
    }

    public function index()
    {
      $data['cinemaList']=$this->Impuesto->getAll();
      // Total and percentage cinemas by cit

      // $data['totalIVA']=$this->Product->getProductsByIVA("desc");
      // $data['totalAverage']=$this->Product->getProductsByAverage();
      $this->load->view('header');
      $this->load->view('impuesto/index',$data);
      $this->load->view('footer');
    }

    public function nuevo()
    {
      $this->load->view('header');
      $this->load->view('impuesto/nuevo');
      $this->load->view('footer');
    }

    public function guardar(){
  	$datosNewProduct=array(
      'nombre_imp'=>$this->input->post('nombre_imp'),
      'descripcion_imp'=>$this->input->post('descripcion_imp'),
  		'porcentaje_imp'=>$this->input->post('porcentaje_imp'),
      'retencion_imp'=>$this->input->post('retencion_imp'),
      'estado_imp'=>$this->input->post('estado_imp'),
  	);
  		if ($this->Impuesto->insertar($datosNewProduct)) {
          redirect('impuestos/index');
  		}
  	}

    public function editar($id_imp){
    $data["productEdit"]=$this->Impuesto->obtenerID($id_imp);
    $this->load->view('header');
    $this->load->view('impuesto/editar',$data);
    $this->load->view('footer');
    }

    public function procesarActualizacion(){
    $datosEditados = array(
      'id_imp'=>$this->input->post('id_imp'),
      'nombre_imp'=>$this->input->post('nombre_imp'),
      'descripcion_imp'=>$this->input->post('descripcion_imp'),
  		'porcentaje_imp'=>$this->input->post('porcentaje_imp'),
      'retencion_imp'=>$this->input->post('retencion_imp'),
      'estado_imp'=>$this->input->post('estado_imp'),
		);
    $id_imp=$this->input->post("id_imp");
      if ($this->Impuesto->actualizar($id_imp,$datosEditados)){
      redirect('impuestos/index');
		}
  }

  public function eliminar($id_imp){
    if ($this->Impuesto->borrar($id_imp)) {
      redirect('impuestos/index');
    }
  }

}
?>

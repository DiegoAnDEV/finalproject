<?php

class Perfiles extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Perfil');
    }

    public function index(){
        //print_r($data);
        $data['listPer']=$this->Perfil->obtenerPerfiles();
        $this->load->view('header');
        $this->load->view('perfiles/index',$data);
        $this->load->view('footer');
    }

    public function nuevo(){
        $this->load->view('header');
        $this->load->view('perfiles/nuevo');
        $this->load->view('footer');
    }

    public function guardar(){
   $datosNewPerfil=array(
     'nombre_per'=>$this->input->post('nombre_per'),
     'descripcion_per'=>$this->input->post('descripcion_per'),
     'estado_per'=>$this->input->post('estado_per')
       );
     if ($this->Perfil->insertar($datosNewPerfil)) {
       redirect('perfiles/index');
     } else {
       echo "Error al guardar, intente otra vez";
     }
   }

   public function eliminar($id_per){
     if ($this->Perfil->borrar($id_per)) {
       redirect('perfiles/index');
      } else {
        echo "Error al eliminar";
      }
     }

   public function editar($id_per){
   $data["perfilEditar"]=$this->Perfil->obtenerID($id_per);
   $this->load->view('header');
   $this->load->view('perfiles/editar',$data);
   $this->load->view('footer');
 }

   public function procesarActualizacion(){
     $datosEditados = array(
       'nombre_per'=>$this->input->post('nombre_per'),
       'descripcion_per'=>$this->input->post('descripcion_per'),
       'estado_per'=>$this->input->post('estado_per')
     );
     $id_per=$this->input->post("id_per");
       if ($this->Perfil->actualizar($id_per,$datosEditados)){
       redirect('perfiles/index');
       } else {
         echo "error " ;
       }
     }


}

<?php
  class Comunicados extends CI_Controller
  {
    function __construct()
    {
      parent::__construct();
      $this->load->model('Comunicado');
    }

    public function index()
    {
      $data['cinemaList']=$this->Comunicado->getAll();
      // Total and percentage cinemas by cit

      // $data['totalIVA']=$this->Product->getProductsByIVA("desc");
      // $data['totalAverage']=$this->Product->getProductsByAverage();
      $this->load->view('header');
      $this->load->view('comunicados/index',$data);
      $this->load->view('footer');
    }

    public function nuevo()
    {
      $this->load->view('header');
      $this->load->view('comunicados/nuevo');
      $this->load->view('footer');
    }

    public function guardar(){
  	$datosNewComunicado=array(
      'fecha_com'=>$this->input->post('fecha_com'),
      'mensaje_com'=>$this->input->post('mensaje_com'),
  		'actualizacion_com'=>$this->input->post('actualizacion_com'),
      'creacion_com'=>$this->input->post('creacion_com'),
  	);
  		if ($this->Comunicado->insertar($datosNewComunicado)) {
          redirect('comunicados/index');
  		}
  	}

    public function editar($id_com){
    $data["comunicadoEdit"]=$this->Comunicado->obtenerID($id_com);
    $this->load->view('header');
    $this->load->view('comunicados/editar',$data);
    $this->load->view('footer');
    }

    public function procesarActualizacion(){
    $datosEditados = array(
      'id_com'=>$this->input->post('id_com'),
      'fecha_com'=>$this->input->post('fecha_com'),
      'mensaje_com'=>$this->input->post('mensaje_com'),
  		'actualizacion_com'=>$this->input->post('actualizacion_com'),
      'creacion_com'=>$this->input->post('creacion_com'),
		);
    $id_com=$this->input->post("id_com");
      if ($this->Comunicado->actualizar($id_com,$datosEditados)){
      redirect('comunicados/index');
		}
  }

  public function eliminar($id_com){
    if ($this->Comunicado->borrar($id_com)) {
      redirect('comunicados/index');
    }
  }

}
?>

<?php
  class Usuarios extends CI_Controller
  {
    function __construct()
    {
      parent::__construct();
      $this->load->model('Usuario');
    }

    public function index()
    {
      $data['cinemaList']=$this->Usuario->getAll();
      $this->load->view('header');
      $this->load->view('usuario/index',$data);
      $this->load->view('footer');
    }

    public function nuevo()
    {
      $this->load->view('header');
      $this->load->view('usuario/nuevo');
      $this->load->view('footer');
    }

    public function guardar(){
  	$datosNewProduct=array(
      'nombre_usu'=>$this->input->post('nombre_usu'),
      'apellido_usu'=>$this->input->post('apellido_usu'),
  		'email_usu'=>$this->input->post('email_usu'),
      'password_usu'=>$this->input->post('password_usu'),
      'estado_usu'=>$this->input->post('estado_usu'),
      'fk_id_per'=>$this->input->post('fk_id_per'),
  	);
  		if ($this->Usuario->insertar($datosNewProduct)) {
          redirect('usuarios/index');
  		}
  	}

    public function editar($id_imp){
    $data["productEdit"]=$this->Usuario->obtenerID($id_imp);
    $this->load->view('header');
    $this->load->view('usuario/editar',$data);
    $this->load->view('footer');
    }

    public function procesarActualizacion(){
    $datosEditados = array(
      'id_usu'=>$this->input->post('id_usu'),
      'nombre_usu'=>$this->input->post('nombre_usu'),
      'apellido_usu'=>$this->input->post('apellido_usu'),
  		'email_usu'=>$this->input->post('email_usu'),
      'password_usu'=>$this->input->post('password_usu'),
      'estado_usu'=>$this->input->post('estado_usu'),
      'fk_id_per'=>$this->input->post('fk_id_per'),
		);
    $id_usu=$this->input->post("id_usu");
      if ($this->Usuario->actualizar($id_usu,$datosEditados)){
      redirect('usuarios/index');
		}
  }

  public function eliminar($id_usu){
    if ($this->Usuario->borrar($id_usu)) {
      redirect('usuarios/index');
    }
  }

}
?>

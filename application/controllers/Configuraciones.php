<?php
  class Configuraciones extends CI_Controller
  {
    function __construct()
    {
      parent::__construct();
      $this->load->model('Configuracion');
    }

    public function index()
    {
      $data['cinemaList']=$this->Configuracion->getAll();
      $this->load->view('header');
      $this->load->view('configuracion/index',$data);
      $this->load->view('footer');
    }

    public function nuevo()
    {
      //$data['tipos']=$this->Evento->getTipos();
      $this->load->view('header');
      $this->load->view('configuracion/nuevo');
      $this->load->view('footer');
    }

    public function guardar(){
  	$datosNewProduct=array(
      'nombre_con'=>$this->input->post('nombre_con'),
      'ruc_con'=>$this->input->post('ruc_con'),
  		'logo_con'=>$this->input->post('logo_con'),
      'telefono_con'=>$this->input->post('telefono_con'),
      'direccion_con'=>$this->input->post('direccion_con'),
      'email_con'=>$this->input->post('email_con'),
      'servidor_con'=>$this->input->post('servidor_con'),
      'puerto_con'=>$this->input->post('puerto_con'),
      'password_con'=>$this->input->post('password_con'),
      'creacion_con'=>$this->input->post('creacion_con'),
      'actualizacion_con'=>$this->input->post('actualizacion_con'),
      'anio_inicial_con'=>$this->input->post('anio_inicial_con'),
      'mes_inicial_con'=>$this->input->post('mes_inicial_con'),
  	);
  		if ($this->Configuracion->insertar($datosNewProduct)) {
          redirect('configuraciones/index');
  		}
  	}

    public function editar($id_con){
    $data["productEdit"]=$this->Configuracion->obtenerID($id_con);
    //$data['tipos']=$this->Evento->getTipos();
    $this->load->view('header');
    $this->load->view('configuracion/editar',$data);
    $this->load->view('footer');
    }

    public function procesarActualizacion(){
    $datosEditados = array(
      'id_con'=>$this->input->post('id_con'),
      'nombre_con'=>$this->input->post('nombre_con'),
      'ruc_con'=>$this->input->post('ruc_con'),
  		'logo_con'=>$this->input->post('logo_con'),
      'telefono_con'=>$this->input->post('telefono_con'),
      'direccion_con'=>$this->input->post('direccion_con'),
      'email_con'=>$this->input->post('email_con'),
      'servidor_con'=>$this->input->post('servidor_con'),
      'puerto_con'=>$this->input->post('puerto_con'),
      'password_con'=>$this->input->post('password_con'),
      'creacion_con'=>$this->input->post('creacion_con'),
      'actualizacion_con'=>$this->input->post('actualizacion_con'),
      'anio_inicial_con'=>$this->input->post('anio_inicial_con'),
      'mes_inicial_con'=>$this->input->post('mes_inicial_con'),
		);
    $id_con=$this->input->post("id_con");
      if ($this->Configuracion->actualizar($id_con,$datosEditados)){
      redirect('configuraciones/index');
		}
  }

  public function eliminar($id_con){
    if ($this->Configuracion->borrar($id_con)) {
      redirect('configuraciones/index');
    }
  }

}
?>

<?php

class Asistencias extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Asistencia');
    }

    public function index(){
        //print_r($data);
        $data['listAsis']=$this->Asistencia->obtenerAsistencias();
        $this->load->view('header');
        $this->load->view('asistencias/index',$data);
        $this->load->view('footer');
    }

    public function nuevo(){
      $data['eventos']=$this->Asistencia->obtenerEventos();
      $data['socios']=$this->Asistencia->obtenerSocios();
        $this->load->view('header');
        $this->load->view('asistencias/nuevo',$data);
        $this->load->view('footer');
    }

    public function guardar(){
   $datosNewAsistencia=array(
     'fk_id_eve'=>$this->input->post('fk_id_eve'),
     'fk_id_soc'=>$this->input->post('fk_id_soc'),
     'tipo_asi'=>$this->input->post('tipo_asi'),
     'valor_asi'=>$this->input->post('valor_asi'),
     'atraso_asi'=>$this->input->post('atraso_asi'),
     'valor_atraso_asi'=>$this->input->post('valor_atraso_asi')
       );
     if ($this->Asistencia->insertar($datosNewAsistencia)) {
       redirect('asistencias/index');
     } else {
       echo "Error al guardar, intente otra vez";
     }
   }

   public function eliminar($id_asi){
     if ($this->Asistencia->borrar($id_asi)) {
       redirect('asistencias/index');
      } else {
        echo "Error al eliminar";
      }
     }

   public function editar($id_asi){
  $data['eventos']=$this->Asistencia->obtenerEventos();
  $data['socios']=$this->Asistencia->obtenerSocios();
   $data["asistenciaEditar"]=$this->Asistencia->obtenerID($id_asi);
   $this->load->view('header');
   $this->load->view('asistencias/editar',$data);
   $this->load->view('footer');
 }

   public function procesarActualizacion(){
     $datosEditados = array(
       'fk_id_eve'=>$this->input->post('fk_id_eve'),
       'fk_id_soc'=>$this->input->post('fk_id_soc'),
       'tipo_asi'=>$this->input->post('tipo_asi'),
       'valor_asi'=>$this->input->post('valor_asi'),
       'atraso_asi'=>$this->input->post('atraso_asi'),
       'valor_atraso_asi'=>$this->input->post('valor_atraso_asi')

     );
     $id_asi=$this->input->post("id_asi");
       if ($this->Asistencia->actualizar($id_asi,$datosEditados)){
       redirect('asistencias/index');
       } else {
         echo "error " ;
       }
     }


}

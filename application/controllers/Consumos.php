<?php

class Consumos extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Consumo');
    }

    public function index(){
        //print_r($data);
        $data['listCon']=$this->Consumo->obtenerConsumos();
        $this->load->view('header');
        $this->load->view('consumos/index',$data);
        $this->load->view('footer');
    }

    public function nuevo(){
        $this->load->view('header');
        $this->load->view('consumos/nuevo');
        $this->load->view('footer');
    }

    public function guardar(){
   $datosNewConsumo=array(
     'anio_consumo'=>$this->input->post('anio_consumo'),
     'estado_consumo'=>$this->input->post('estado_consumo'),
     'mes_consumo'=>$this->input->post('mes_consumo'),
     'numero_mes_consumo'=>$this->input->post('numero_mes_consumo'),
     'fecha_vencimiento_consumo'=>$this->input->post('fecha_vencimiento_consumo')
     );
     if ($this->Consumo->insertar($datosNewConsumo)) {
       redirect('consumos/index');
     } else {
       echo "Error al guardar, intente otra vez";
     }
   }

   public function eliminar($id_consumo){
     if ($this->Consumo->borrar($id_consumo)) {
       redirect('consumos/index');
      } else {
        echo "Error al eliminar";
      }
     }

   public function editar($id_consumo){
   $data["consumoEditar"]=$this->Consumo->obtenerID($id_consumo);
   $this->load->view('header');
   $this->load->view('consumos/editar',$data);
   $this->load->view('footer');
 }

   public function procesarActualizacion(){
     $datosEditados = array(
       'anio_consumo'=>$this->input->post('anio_consumo'),
       'estado_consumo'=>$this->input->post('estado_consumo'),
       'mes_consumo'=>$this->input->post('mes_consumo'),
       'numero_mes_consumo'=>$this->input->post('numero_mes_consumo'),
       'fecha_vencimiento_consumo'=>$this->input->post('fecha_vencimiento_consumo')
     );
     $id_consumo=$this->input->post("id_consumo");
       if ($this->Consumo->actualizar($id_consumo,$datosEditados)){
       redirect('consumos/index');
       } else {
         echo "error " ;
       }
     }


}

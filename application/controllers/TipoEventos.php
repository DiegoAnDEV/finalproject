<?php

class TipoEventos extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('TipoEvento');
    }

    public function index(){
        //print_r($data);
        $data['listPer']=$this->TipoEvento->obtenerTipoEventos();
        $this->load->view('header');
        $this->load->view('tipoeventos/index',$data);
        $this->load->view('footer');
    }

    public function nuevo(){
        $this->load->view('header');
        $this->load->view('tipoeventos/nuevo');
        $this->load->view('footer');
    }

    public function guardar(){
   $datosNewTipoEvento=array(
     'nombre_te'=>$this->input->post('nombre_te'),
     'estado_te'=>$this->input->post('estado_te')
       );
     if ($this->TipoEvento->insertar($datosNewTipoEvento)) {
       redirect('TipoEventos/index');
     } else {
       echo "Error al guardar, intente otra vez";
     }
   }

   public function eliminar($id_te){
     if ($this->TipoEvento->borrar($id_te)) {
       redirect('TipoEventos/index');
      } else {
        echo "Error al eliminar";
      }
     }

   public function editar($id_te){
   $data["tipoEventoEditar"]=$this->TipoEvento->obtenerID($id_te);
   $this->load->view('header');
   $this->load->view('tipoeventos/editar',$data);
   $this->load->view('footer');
 }

   public function procesarActualizacion(){
     $datosEditados = array(
       'nombre_te'=>$this->input->post('nombre_te'),
       'estado_te'=>$this->input->post('estado_te')
     );
     $id_te=$this->input->post("id_te");
       if ($this->TipoEvento->actualizar($id_te,$datosEditados)){
       redirect('TipoEventos/index');
       } else {
         echo "error " ;
       }
     }


}

<?php
  class Rutas extends CI_Controller
  {
    function __construct()
    {
      parent::__construct();
      $this->load->model('Ruta');
    }

    public function index()
    {
      $data['cinemaList']=$this->Ruta->getAll();
      // Total and percentage cinemas by cit

      // $data['totalIVA']=$this->Product->getProductsByIVA("desc");
      // $data['totalAverage']=$this->Product->getProductsByAverage();
      $this->load->view('header');
      $this->load->view('rutas/index',$data);
      $this->load->view('footer');
    }

    public function nuevo()
    {
      $this->load->view('header');
      $this->load->view('rutas/nuevo');
      $this->load->view('footer');
    }

    public function guardar(){
  	$datosNewRuta=array(
      'nombre_rut'=>$this->input->post('nombre_rut'),
      'descripcion_rut'=>$this->input->post('descripcion_rut'),
  		'estado_rut'=>$this->input->post('estado_rut'),
  	);
  		if ($this->Ruta->insertar($datosNewRuta)) {
          redirect('rutas/index');
  		}
  	}

    public function editar($id_rut){
    $data["rutaEdit"]=$this->Ruta->obtenerID($id_rut);
    $this->load->view('header');
    $this->load->view('rutas/editar',$data);
    $this->load->view('footer');
    }

    public function procesarActualizacion(){
    $datosEditados = array(
      'id_rut'=>$this->input->post('id_rut'),
      'nombre_rut'=>$this->input->post('nombre_rut'),
      'descripcion_rut'=>$this->input->post('descripcion_rut'),
      'estado_rut'=>$this->input->post('estado_rut'),
		);
    $id_rut=$this->input->post("id_rut");
      if ($this->Ruta->actualizar($id_rut,$datosEditados)){
      redirect('rutas/index');
		}
  }

  public function eliminar($id_rut){
    if ($this->Ruta->borrar($id_rut)) {
      redirect('rutas/index');
    }
  }

}
?>

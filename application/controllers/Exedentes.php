<?php
  class Exedentes extends CI_Controller
  {
    function __construct()
    {
      parent::__construct();
      $this->load->model('Exedente');
    }

    public function index()
    {
      $data['cinemaList']=$this->Exedente->getAll();
      // Total and percentage cinemas by cit

      // $data['totalIVA']=$this->Product->getProductsByIVA("desc");
      // $data['totalAverage']=$this->Product->getProductsByAverage();
      $this->load->view('header');
      $this->load->view('exedentes/index',$data);
      $this->load->view('footer');
    }

    public function nuevo()
    {
      $this->load->view('header');
      $this->load->view('exedentes/nuevo');
      $this->load->view('footer');
    }

    public function guardar(){
  	$datosNewExedente=array(
      'id_tar'=>$this->input->post('id_tar'),
      'limite_minimo_ex'=>$this->input->post('limite_minimo_ex'),
      'limite_maximo_ex'=>$this->input->post('limite_maximo_ex'),
  		'tarifa_ex'=>$this->input->post('tarifa_ex'),
      'fecha_actualizacion_ex'=>$this->input->post('fecha_actualizacion_ex'),
      'fecha_creacion_ex'=>$this->input->post('fecha_creacion_ex'),
  	);
  		if ($this->Exedente->insertar($datosNewExedente)) {
          redirect('exedentes/index');
  		}
  	}

    public function editar($id_ex){
    $data["exdenteEdit"]=$this->Exedente->obtenerID($id_ex);
    $this->load->view('header');
    $this->load->view('exedentes/editar',$data);
    $this->load->view('footer');
    }

    public function procesarActualizacion(){
    $datosEditados = array(
      'id_ex'=>$this->input->post('id_ex'),
      'limite_minimo_ex'=>$this->input->post('limite_minimo_ex'),
      'limite_maximo_ex'=>$this->input->post('limite_maximo_ex'),
  		'tarifa_ex'=>$this->input->post('tarifa_ex'),
      'fecha_actualizacion_ex'=>$this->input->post('fecha_actualizacion_ex'),
      'fecha_creacion_ex'=>$this->input->post('fecha_creacion_ex'),
		);
    $id_ex=$this->input->post("id_ex");
      if ($this->Exedente->actualizar($id_ex,$datosEditados)){
      redirect('exedentes/index');
		}
  }

  public function eliminar($id_ex){
    if ($this->Exedente->borrar($id_ex)) {
      redirect('exedentes/index');
    }
  }

}
?>

<div class="content">
  <div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
          <legend>LISTA DE PERFILES</legend>
          <br>
          <div class="row">
            <div class="col-md-1 ">
              <a class="btn btn-secondary"   href="<?php echo site_url("Perfiles/nuevo");?>">NUEVO</a>
            </div>
          </div>
          <br>
          <?php if($listPer): ?>
            <table id="tperfil" class="table table-striped">
              <thead>
                <tr class="table table-primary">
                  <td>ID</td>
                  <td>NOMBRE</td>
                  <td>ESTADO</td>
                  <td>DESCRIPCIÓN</td>
                  <td>CREACIÓN</td>
                  <td>ACTUALIZACIÓN</td>
                  <td >ACCIONES</td>
                </tr>
              </thead>
              <tbody>
              <?php foreach ($listPer as $p):?>
                <tr>
                  <td><?php echo $p->id_per?></td>
                  <td><?php echo $p->nombre_per?></td>
                  <td><?php echo $p->estado_per?></td>
                  <td><?php echo $p->descripcion_per?></td>
                  <td><?php echo $p->creacion_per?></td>
                  <td><?php echo $p->actualizacion_per?></td>
                  <td>
                    <a class="btn btn-info" style="margin:0.1rem"  href="<?php echo site_url("/Perfiles/editar/$p->id_per");?>">EDITAR</a>
                    <a class="btn btn-danger" style="margin:0.1rem" href="<?php echo site_url("/Perfiles/eliminar/$p->id_per");?>" >ELIMINAR</a>
                  </td>
                </tr>
              <?php endforeach; ?>
              </tbody>
            </table>
          <?php else: ?>
            <h3>No Asistencias in database</h3>
          <?php endif; ?>
        </div>

      </div>
      <script type="text/javascript">
      new DataTable('#tperfil', {
        layout: {
            topStart: {
                buttons: [
                    {
                        extend: 'pdf',
                        text: 'Reporte PDF',
                    },
                    {
                        extend: 'print',
                        text: 'Reporte Imprimir',
                    },
                    {
                        extend: 'excel',
                        text: 'Reporte Excel',
                    },
                    {
                        extend: 'csv', // Cambiar a 'csv' para generar un archivo CSV
                        text: 'Reporte CSV', // Cambiar el texto del botón
                    }
                ]
            }
        }
      });
      </script>


    </div>
  </div>

</div>

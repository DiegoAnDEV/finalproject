<div class="content">
  <div class="container-fluid">
    <div class="row">

      <form class="form-control " id="form_nuevo" action="<?php echo site_url('Perfiles/procesarActualizacion');?>" method="post" enctype="multipart/form-data">
        <br>
        <h2 class="text-center">EDITAR PERFIL</h2>

        <br>
        <div class="row">
          <div class="col-md-6">
              <label for="">ID:</label>
              <br>
              <input type="text" class="form-control" name="id_per" id="id_per" value="<?php echo $perfilEditar->id_per?>" disabled>
            </div>
          <div class="col-md-6">

            <label for="">NOMBRE:</label>
            <br>
            <input type="text" class="form-control" name="nombre_per" id="nombre_per" placeholder="Ingrese nombre" value="<?php echo $perfilEditar->nombre_per?>" required>
          </div>

        </div>
        <br>
        <div class="row">
          <div class="col-md-6">
            <label for="">ESTADO:</label>
            <br>
            <select class="form-control"  name="estado_per" id="estado_per" value="<?php echo $perfilEditar->estado_per?>" required>
              <option value="Activa">Activa</option>
              <option value="Inactiva">Inactiva</option>
            </select>
          </div>
            <div class="col-md-6">
              <label for="">DESCRIPCIÓN</label>
              <input type="text" class="form-control" name="descripcion_per" id="descripcion_per" placeholder="Ingrese descripción" value="<?php echo $perfilEditar->descripcion_per?>" required>
            </div>
        </div>
        <br>

        <div class="row">
          <button type="submit" class="btn btn-info col-md-5" style="margin:1rem">GUARDAR</button>
          <a href="<?php echo site_url()?>/Perfiles/index" class="btn btn-danger col-md-5" style="margin:1rem" >CANCELAR</a>
        </div>
      </form>
    </div>
  </div>

</div>

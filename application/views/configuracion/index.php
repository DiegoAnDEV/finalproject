<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-12">
        <legend>CONFIGURACIÓN</legend>
        <a class="btn btn-success" href="<?php echo site_url('Configuraciones/nuevo');?>">Nueva Configuración</a>
        <?php if ($cinemaList):?>
          <table class="table table-hover table-dark" id="tblProd" name="tblProd">
            <thead>
              <tr>
                <th>ID</th>
                <th>Nombre</th>
                <th>RUC</th>
                <th>Logo</th>
                <th>Telefono</th>
                <th>Direccion</th>
                <th>Email</th>
                <th>Servidor</th>
                <th>Puerto</th>
                <th>Clave</th>
                <th>Creacion</th>
                <th>Actualizacion</th>
                <th>Año Inicial</th>
                <th>Mes Inicial</th>
                <th>Acciones</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($cinemaList as $p): ?>
                <tr>
                  <td><?php echo $p->id_con?></td>
                  <td><?php echo $p->nombre_con?></td>
                  <td><?php echo $p->ruc_con?></td>
                  <td><?php echo $p->logo_con?></td>
                  <td><?php echo $p->telefono_con?></td>
                  <td><?php echo $p->direccion_con?></td>
                  <td><?php echo $p->email_con?></td>
                  <td><?php echo $p->servidor_con?></td>
                  <td><?php echo $p->puerto_con?></td>
                  <td><?php echo $p->password_con?></td>
                  <td><?php echo $p->creacion_con?></td>
                  <td><?php echo $p->actualizacion_con?></td>
                  <td><?php echo $p->anio_inicial_con?></td>
                  <td><?php echo $p->mes_inicial_con?></td>
                  <td class="text-center">
                    <div class="">
                      <a class="btn btn-success" style="color: white;font-size: 16px;" href="<?php echo site_url();?>/configuraciones/editar/<?php echo $p->id_con;?>" title="Editar">
                        Editar
                      </a>
                      &nbsp;&nbsp;&nbsp;&nbsp;
                      <a class="btn btn-danger" style="color:red;font-size: 16px; color:white"  href="<?php echo site_url();?>/configuraciones/eliminar/<?php echo $p->id_con;?>" title="Borrar ">
                        Borrar
                      </a>
                    </div>
                  </td>
                </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
        <?php else: ?>
          <h3>No data in the Database</h3>
        <?php endif; ?>
      </div>
</div>
<script type="text/javascript">
new DataTable('#tblProd', {
  layout: {
      topStart: {
          buttons: [
              {
                  extend: 'pdf',
                  text: 'Reporte PDF',
              },
              {
                  extend: 'print',
                  text: 'Reporte Imprimir',
              },
              {
                  extend: 'excel',
                  text: 'Reporte Excel',
              },
              {
                  extend: 'csv', // Cambiar a 'csv' para generar un archivo CSV
                  text: 'Reporte CSV', // Cambiar el texto del botón
              }
          ]
      }
  }
});
</script>
<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>

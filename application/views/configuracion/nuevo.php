<div class="content">
  <!-- Form Start -->
  <div class="container-fluid pt-4 px-4">
      <div class="row g-4">
          <div class="col-sm-12 col-xl-6">
              <div class="bg-secondary rounded h-100 p-4">
                  <h6 class="mb-4">Nueva Configuracion</h6>
                  <form action="<?php echo site_url('Configuraciones/guardar');?>" method="post" role="form" enctype="multipart/form-data">
                      <div class="mb-3">
                          <label for="exampleInputEmail1" class="form-label">Nombre</label>
                          <input type="text" class="form-control" id="nombre_con" required>
                      </div>
                      <div class="mb-3">
                          <label for="exampleInputEmail1" class="form-label">RUC</label>
                          <input type="text" class="form-control" id="ruc_con" required>
                      </div>
                      <div class="mb-3">
                          <label for="exampleInputEmail1" class="form-label">Logo</label>
                          <input type="text" class="form-control" id="logo_con" required>
                      </div>
                      <div class="mb-3">
                          <label for="exampleInputEmail1" class="form-label">Telefono</label>
                          <input type="text" class="form-control" id="telefono_con" required>
                      </div>
                      <div class="mb-3">
                          <label for="exampleInputEmail1" class="form-label">Direccion</label>
                          <input type="text" class="form-control" id="direccion_con" required>
                      </div>
                      <div class="mb-3">
                          <label for="exampleInputEmail1" class="form-label">Email</label>
                          <input type="text" class="form-control" id="email_con" required>
                      </div>
                      <div class="mb-3">
                          <label for="exampleInputEmail1" class="form-label">Servidor</label>
                          <input type="text" class="form-control" id="servidor_con" required>
                      </div>
                      <div class="mb-3">
                          <label for="exampleInputEmail1" class="form-label">Puerto</label>
                          <input type="text" class="form-control" id="puerto_con" required>
                      </div>
                      <div class="mb-3">
                          <label for="exampleInputEmail1" class="form-label">Clave</label>
                          <input type="text" class="form-control" id="password_con" required>
                      </div>
                      <div class="mb-3">
                          <label for="exampleInputEmail1" class="form-label">Fecha de Creacion</label>
                          <input type="datetime-local" class="form-control" id="creacion_con" required>
                      </div>
                      <div class="mb-3">
                          <label for="exampleInputEmail1" class="form-label">Fecha de Actualizacion</label>
                          <input type="datetime-local" class="form-control" id="actualizacion_con" required>
                      </div>
                      <div class="mb-3">
                          <label for="exampleInputEmail1" class="form-label">Año Inicial</label>
                          <input type="number" class="form-control" id="anio_inicial_con" required>
                      </div>
                      <div class="mb-3">
                          <div class="mb-3">
                              <label class="form-label">Mes Inicial</label>
                              <select class="form-select" name="mes_inicial_con" id="mes_inicial_con" required>
                                <option value="Enero">Enero</option>
                                <option value="Febrero">Febrero</option>
                                <option value="Marzo">Marzo</option>
                                <option value="Abril">Abril</option>
                                <option value="Mayo">Mayo</option>
                                <option value="Junio">Junio</option>
                                <option value="Julio">Julio</option>
                                <option value="Agosto">Agosto</option>
                                <option value="Septiembre">Septiembre</option>
                                <option value="Octubre">Octubre</option>
                                <option value="Noviembre">Noviembre</option>
                                <option value="Diciembre">Diciembre</option>
                              </select>
                          </div>
                      </div>
                      <button type="submit" class="btn btn-primary">Guardar</button>
                      <a href="<?php echo site_url()?>/Configuraciones/index" type="submit" class="btn btn-danger">Cancelar</a>
                  </form>
              </div>
          </div>

      </div>
  </div>
  <!-- Form End -->
</div>

<div class="content">
  <form class="form-control col-md-8" action="<?php echo site_url('comunicados/guardar');?>" method="post" role="form" enctype="multipart/form-data">
    <div class="row">
      <div class="col-md-4" style="padding:1rem;">
        <div class="form-group">
          <label for="fecha_com">Fecha:</label>
          <input class="form-control" type="date" name="fecha_com" id="fecha_com" required>
        </div>
      </div>
      <div class="col-md-4" style="padding:1rem;">
        <div class="form-group">
          <label for="mensaje_com">Mensaje:</label>
          <input class="form-control" type="text" name="mensaje_com" id="mensaje_com" required>
        </div>
      </div>
      <div class="col-md-4" style="padding:1rem;">
        <div class="form-group">
          <label for="creacion_com">Creacion:</label>
          <input class="form-control" type="datetime-local" name="creacion_com" id="creacion_com" readonly required>
        </div>
      </div>
    </div>

    <div class="row mt-3">
      <div class="col-md-4 mx-auto">
        <button type="submit" class="btn btn-info form-control">Guardar</button>
      </div>
      <div class="col-md-4 mx-auto">
        <a href="<?php echo site_url()?>/comunicados/index" type="submit" class="btn btn-danger form-control  text-dark">Cancelar</a>
      </div>
    </div>
  </form>
</div>

<script>
  // Obtener la fecha y hora actual en el formato deseado (YYYY-MM-DDTHH:MM)
  var currentDate = new Date();
  var year = currentDate.getFullYear();
  var month = ("0" + (currentDate.getMonth() + 1)).slice(-2); // Agrega 1 porque los meses van de 0 a 11
  var day = ("0" + currentDate.getDate()).slice(-2);
  var hours = ("0" + currentDate.getHours()).slice(-2);
  var minutes = ("0" + currentDate.getMinutes()).slice(-2);
  var currentDateTime = year + "-" + month + "-" + day + "T" + hours + ":" + minutes;

  // Insertar la fecha y hora actual en el campo de creación
  document.getElementById('creacion_com').value = currentDateTime;
</script>

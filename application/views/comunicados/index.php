<div class="content">
  <div class="container-fluid">
    <div class="row">

      <div class="col-md-12">
        <legend>Lista de Comunicados</legend>
        <a class="btn btn-success" href="<?php echo site_url('comunicados/nuevo');?>">Nuevo Comunicado</a>
        <?php if ($cinemaList):?>
          <table class="table table-hover table-dark" id="tblProd" name="tblProd">
            <thead>
              <tr>
                <th>ID</th>
                <th>Fecha</th>
                <th>Mensaje</th>
                <th>Actualizacion</th>
                <th>Creacion</th>
                <th>Acciones</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($cinemaList as $p): ?>
                <tr>
                  <td><?php echo $p->id_com?></td>
                  <td><?php echo $p->fecha_com?></td>
                  <td><?php echo $p->mensaje_com?></td>
                  <td><?php echo $p->actualizacion_com?></td>
                  <td><?php echo $p->creacion_com?></td>
                  <td class="text-center">
                    <div class="">
                      <a class="btn btn-success" style="color: white;font-size: 16px;" href="<?php echo site_url();?>/comunicados/editar/<?php echo $p->id_com;?>" title="Edit Cinema">
                        Editar
                      </a>
                      &nbsp;&nbsp;&nbsp;&nbsp;
                      <a class="btn btn-danger" style="color:red;font-size: 16px; color:white"  href="<?php echo site_url();?>/comunicados/eliminar/<?php echo $p->id_com;?>" title="Delete ">
                        eliminar
                      </a>
                    </div>
                  </td>
                </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
        <?php else: ?>
          <h3>No taxes in the Database</h3>
        <?php endif; ?>
      </div>
</div>
<script type="text/javascript">
new DataTable('#tblProd', {
  layout: {
      topStart: {
          buttons: [
              {
                  extend: 'pdf',
                  text: 'Reporte PDF',
              },
              {
                  extend: 'print',
                  text: 'Reporte Imprimir',
              },
              {
                  extend: 'excel',
                  text: 'Reporte Excel',
              },
              {
                  extend: 'csv', // Cambiar a 'csv' para generar un archivo CSV
                  text: 'Reporte CSV', // Cambiar el texto del botón
              }
          ]
      }
  }
});
</script>
<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>

<div class="content">
  <form class="form-control col-md-8" action="<?php echo site_url('comunicados/procesarActualizacion');?>" method="post" role="form" enctype="multipart/form-data">
    <div class="col-md-3" style="padding:1rem;">
      <label for="">Fecha:</label>
      <input class="form-control" type="text" name="id_com" id="id_com" value="<?php echo $comunicadoEdit->id_com?>" hidden>
      <input class="form-control" type="text" name="fecha_com" id="fecha_com" value="<?php echo $comunicadoEdit->fecha_com?>">
    </div>
    <div class="col-md-3" style="padding:1rem;">
      <label for="">Mensaje:</label>
      <input class="form-control" type="text" name="mensaje_com" id="mensaje_com" value="<?php echo $comunicadoEdit->mensaje_com?>">
    </div>
    <div class="col-md-3" style="padding:1rem;">
      <label for="">Actualizacion:</label>
      <input class="form-control" type="text" name="actualizacion_com" id="actualizacion_com">
    </div>
    <div class="col-md-3" style="padding:1rem;">
      <label for="">Creacion:</label>
      <input class="form-control" type="text" name="creacion_com" id="creacion_com" value="<?php echo $comunicadoEdit->creacion_com?>" readonly>
    </div>

    <div class="col-4 mx-auto">
      <button type="submit" class="btn btn-info form-control">SAVE</button>
    </div>
    <div class="col-4 mx-auto">
      <a href="<?php echo site_url()?>/comunicados/index" type="submit" class="btn btn-danger form-control">CANCEL</a>
    </div>
  </form>
</div>

<script>
  // Obtener la fecha y hora actual en el formato deseado (YYYY-MM-DD HH:MM:SS)
  var currentDate = new Date();
  var year = currentDate.getFullYear();
  var month = ("0" + (currentDate.getMonth() + 1)).slice(-2); // Agrega 1 porque los meses van de 0 a 11
  var day = ("0" + currentDate.getDate()).slice(-2);
  var hours = ("0" + currentDate.getHours()).slice(-2);
  var minutes = ("0" + currentDate.getMinutes()).slice(-2);
  var seconds = ("0" + currentDate.getSeconds()).slice(-2);
  var currentDateTime = year + "-" + month + "-" + day + " " + hours + ":" + minutes + ":" + seconds;

  // Insertar la fecha y hora actual en el campo de actualización
  document.getElementById('actualizacion_com').value = currentDateTime;
</script>

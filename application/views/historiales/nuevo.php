<div class="content">
  <form class="col-md-8" action="<?php echo site_url('historiales/guardar');?>" method="post" role="form" enctype="multipart/form-data">
    <div class="row">
      <div class="col-md-6">
        <div class="form-group">
          <label for="fk_id_med">Elija el Número del Medidor</label>
          <select class="form-control" name="fk_id_med" id="fk_id_med" required>
            <?php foreach ($historia as $opcionTemporal): ?>
              <option value="<?php echo $opcionTemporal->id_med?>"> <?php echo $opcionTemporal->numero_med?> </option>
            <?php endforeach; ?>
          </select>
        </div>
        <div class="form-group">
          <label for="fk_id_soc">Elija el Socio</label>
          <select class="form-control" name="fk_id_soc" id="fk_id_soc" required>
            <?php foreach ($socio as $opcionTemporal): ?>
              <option value="<?php echo $opcionTemporal->id_soc?>"> <?php echo $opcionTemporal->nombres_soc?> <?php echo $opcionTemporal->primer_apellido_soc?> </option>
            <?php endforeach; ?>
          </select>
        </div>
        <div class="form-group">
          <label for="estado_his">Estado:</label>
          <select class="form-control" name="estado_his" id="estado_his" required>
            <option value="" disabled selected>Selecciona un estado</option>
            <option value="ACTIVO">ACTIVO</option>
            <option value="INACTIVO">INACTIVO</option>
          </select>
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
          <label for="observacion_his">Observaciones:</label>
          <input class="form-control" type="text" name="observacion_his" id="observacion_his" required>
        </div>
        <div class="form-group">
          <label for="fecha_cambio_his">Fecha de Cambio Medidor:</label>
          <input class="form-control" type="text" name="fecha_cambio_his" id="fecha_cambio_his" readonly required>
        </div>
        <div class="form-group">
          <label for="creacion_his">Creación Historial de Propietario:</label>
          <input class="form-control" type="text" name="creacion_his" id="creacion_his" readonly required>
        </div>
        <div class="form-group">
          <label for="propietario_actual_his">Propietario Actual:</label>
          <input class="form-control" type="text" name="propietario_actual_his" id="propietario_actual_his" required>
        </div>
      </div>
    </div>
    <div class="row mt-3">
      <div class="col-md-6">
        <button type="submit" class="btn btn-info form-control">GUARDAR</button>
      </div>
      <div class="col-md-6">
        <a href="<?php echo site_url()?>/historiales/index" type="submit" class="btn btn-danger form-control  text-dark">CANCELAR</a>
      </div>
    </div>
  </form>
</div>

<script>
  // Obtener la fecha y hora actual
  var currentDate = new Date();

  // Formatear la fecha y hora actual
  var formattedDateTime = currentDate.toISOString().slice(0, 19).replace('T', ' '); // Formato: YYYY-MM-DD HH:MM:SS

  // Insertar la fecha y hora actual en los campos de creación y actualización
  document.getElementById('fecha_cambio_his').value = formattedDateTime;
  document.getElementById('creacion_his').value = formattedDateTime;
</script>

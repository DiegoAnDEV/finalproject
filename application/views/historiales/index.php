<div class="content">
  <div class="container-fluid">
    <div class="row">

      <div class="col-md-12">
        <legend>Lista de Historial de Propietarios</legend>
        <a class="btn btn-success" href="<?php echo site_url('historiales/nuevo');?>">Nuevo Historial</a>
        <?php if ($cinemaList):?>
          <table class="table table-hover table-dark" id="tblProd" name="tblProd">
            <thead>
              <tr>
                <th>ID</th>
                <th>Medidor</th>
                <th>Socio</th>
                <th>Actualizacion</th>
                <th>Estado</th>
                <th>Observaciones</th>
                <th>Fecha de Cambio</th>
                <th>Creacion</th>
                <th>Propietario Actual</th>
                <th>Acciones</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($cinemaList as $p): ?>
                <tr>
                  <td><?php echo $p->id_his?></td>
                  <td><?php echo $p->fk_id_med?></td>
                  <td><?php echo $p->fk_id_soc?></td>
                  <td><?php echo $p->actualizacion_his?></td>
                  <td><?php echo $p->estado_his?></td>
                  <td><?php echo $p->observacion_his?></td>
                  <td><?php echo $p->fecha_cambio_his?></td>
                  <td><?php echo $p->creacion_his?></td>
                  <td><?php echo $p->propietario_actual_his?></td>
                  <td class="text-center">
                    <div class="">
                      <a class="btn btn-success" style="color: white;font-size: 16px;" href="<?php echo site_url();?>/historiales/editar/<?php echo $p->id_his;?>" title="Edit Cinema">
                        Editar
                      </a>
                      &nbsp;&nbsp;&nbsp;&nbsp;
                      <a class="btn btn-danger" style="color:red;font-size: 16px; color:white"  href="<?php echo site_url();?>/historiales/eliminar/<?php echo $p->id_his;?>" title="Delete ">
                        eliminar
                      </a>
                    </div>
                  </td>
                </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
        <?php else: ?>
          <h3>No taxes in the Database</h3>
        <?php endif; ?>
      </div>
</div>

<script type="text/javascript">
new DataTable('#tblProd', {
  layout: {
      topStart: {
          buttons: [
              {
                  extend: 'pdf',
                  text: 'Reporte PDF',
              },
              {
                  extend: 'print',
                  text: 'Reporte Imprimir',
              },
              {
                  extend: 'excel',
                  text: 'Reporte Excel',
              },
              {
                  extend: 'csv', // Cambiar a 'csv' para generar un archivo CSV
                  text: 'Reporte CSV', // Cambiar el texto del botón
              }
          ]
      }
  }
});
</script>
<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>

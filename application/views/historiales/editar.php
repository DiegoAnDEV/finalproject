<div class="content">
  <form class="form-control col-md-8" action="<?php echo site_url('historiales/procesarActualizacion');?>" method="post" role="form" enctype="multipart/form-data">
    <div class="col-md-3" style="padding:1rem;">
      <label for="">ID Medidor:</label>
      <input class="form-control" type="text" name="id_his" id="id_his" value="<?php echo $historialEdit->id_his?>" hidden>
      <input class="form-control" type="text" name="fk_id_med" id="fk_id_med" value="<?php echo $historialEdit->fk_id_med?>" >
    </div>
    <div class="col-md-3" style="padding:1rem;">
      <label for="">ID Socio:</label>
      <input class="form-control" type="text" name="fk_id_soc" id="fk_id_soc" value="<?php echo $historialEdit->fk_id_soc?>" >
    </div>
    <div class="col-md-3" style="padding:1rem;">
      <label for="">Actualizacion:</label>
      <input class="form-control" type="text" name="actualizacion_his" id="actualizacion_his" value="<?php echo $historialEdit->actualizacion_his?>" readonly>
    </div>
    <div class="col-md-3" style="padding:1rem;">
      <label for="">Estado:</label>
      <select class="form-control" name="estado_his" id="estado_his" value="<?php echo $historialEdit->estado_his?>" >
        <option value="ACTIVO">ACTIVO</option>
        <option value="INACTIVO">INACTIVO</option>
      </select>
    </div>
    <div class="col-md-3" style="padding:1rem;">
      <label for="">Observaciones:</label>
      <input class="form-control" type="text" name="observacion_his" id="observacion_his" value="<?php echo $historialEdit->observacion_his?>" >
    </div>
    <div class="col-md-3" style="padding:1rem;">
      <label for="">Fecha de Cambio Medidor:</label>
      <input class="form-control" type="text" name="fecha_cambio_his" id="fecha_cambio_his" value="<?php echo $historialEdit->fecha_cambio_his?>"  readonly>
    </div>
    <div class="col-md-3" style="padding:1rem;">
      <label for="">Creacion Historial de Propietario:</label>
      <input class="form-control" type="text" name="creacion_his" id="creacion_his" value="<?php echo $historialEdit->creacion_his?>"  readonly>
    </div>
    <div class="col-md-3" style="padding:1rem;">
      <label for="">Propietario Actual:</label>
      <input class="form-control" type="text" name="propietario_actual_his" id="propietario_actual_his" value="<?php echo $historialEdit->propietario_actual_his?>" >
    </div>
    <div class="col-4 mx-auto">
      <button type="submit" class="btn btn-info form-control">SAVE</button>
    </div>
    <div class="col-4 mx-auto">
      <a href="<?php echo site_url()?>/historiales/index" type="submit" class="btn btn-danger form-control">CANCEL</a>

    </div>
  </form>
</div>
<script>
  // Obtener la fecha y hora actual
  var currentDate = new Date();

  // Formatear la fecha y hora actual
  var formattedDateTime = currentDate.toISOString().slice(0, 19).replace('T', ' '); // Formato: YYYY-MM-DD HH:MM:SS

  // Insertar la fecha y hora actual en los campos de creación y actualización
  document.getElementById('fecha_cambio_his').value = formattedDateTime;
  document.getElementById('actualizacion_his').value = formattedDateTime;
</script>

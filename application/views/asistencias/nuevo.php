<div class="content">
  <div class="container-fluid">
    <div class="row">

      <form class="form-control " id="form_nuevo" action="<?php echo site_url('Asistencias/guardar');?>" method="post" enctype="multipart/form-data">
        <br>
        <h2 class="text-center">NUEVA ASISTENCIA</h2>
        <br>
        <div class="row">

          <div class="col-md-6">
            <label for="">EVENTO:</label>
            <select class="form-control" name="fk_id_eve" id="fk_id_eve" required>
                  <?php foreach ($eventos as $opcionTemporal){ ?>
                  <option value="<?php echo $opcionTemporal->id_eve?>"> <?php echo $opcionTemporal->descripcion_eve?> </option>
                <?php };?>
            </select>
          </div>
          <div class="col-md-6">
            <label for="">SOCIO:</label>
            <br>
            <select class="form-control" name="fk_id_soc" id="fk_id_soc" required>
                  <?php foreach ($socios as $opcionTemporal){ ?>
                  <option value="<?php echo $opcionTemporal->id_soc?>"> <?php echo $opcionTemporal->nombres_soc?> </option>
                <?php };?>
            </select>
          </div>
        </div>
        <br>
        <div class="row">
          <div class="col-md-6">
            <label for="">TIPO:</label>
            <br>
            <select class="form-control"  name="tipo_asi" id="tipo_asi" required>
              <option value="SOCIO">SOCIO</option>
              <option value="FALTA">FALTA</option>
            </select>
          </div>
          <div class="col-md-6">
            <label for="">VALOR:</label>
            <br>
            <input type="number" class="form-control" name="valor_asi" id="valor_asi" placeholder="Ingrese valor" required>
          </div>

        </div>
        <br>
        <div class="row">
            <div class="col-md-6">
              <label for="">ATRASO</label>
              <select class="form-control"  name="atraso_asi" id="atraso_asi" required>
                <option value="SI">SI</option>
                <option value="NO">NO</option>
              </select>
            </div>
            <div class="col-md-6">
              <label for="">VALOR ATRASO</label>
              <input type="number"  class="form-control" name="valor_atraso_asi"  id="valor_atraso_asi" placeholder="Ingrese valor" required>
            </div>
        </div>
        <br>
        <div class="row">
          <button type="submit" class="btn btn-info col-md-5" style="margin:1rem">GUARDAR</button>
          <a href="<?php echo site_url()?>/Asistencias/index" class="btn btn-danger col-md-5" style="margin:1rem" >CANCELAR</a>
        </div>
      </form>
    </div>
  </div>

</div>

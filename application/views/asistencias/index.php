<div class="content">
<div class="container-fluid">
  <div class="row">
      <div class="col-md-12">
        <legend>LISTA DE ASISTENCIAS</legend>
        <br>
        <div class="row">
          <div class="col-md-1 ">
            <a class="btn btn-secondary"   href="<?php echo site_url("Asistencias/nuevo");?>">NUEVO</a>
          </div>
        </div>
        <br>
        <?php if($listAsis): ?>
          <table id="tproduc" class="table table-striped">
            <thead>
              <tr class="table table-primary">
                <td>ID</td>
                <td>ATRASO</td>
                <td>TIPO</td>
                <td>VALOR</td>
                <td>CREACIÓN</td>
                <td>ACTUALIZACIÓN</td>
                <td >ACCIONES</td>
              </tr>
            </thead>
            <tbody>
            <?php foreach ($listAsis as $p):?>
              <tr>
                <td><?php echo $p->id_asi?></td>
                <td><?php echo $p->atraso_asi?></td>
                <td><?php echo $p->tipo_asi?></td>
                <td><?php echo $p->valor_asi?></td>
                <td><?php echo $p->creacion_asi?></td>
                <td><?php echo $p->actualizacion_asi?></td>
                <td>
                  <a class="btn btn-info" style="margin:0.1rem"  href="<?php echo site_url("/Asistencias/editar/$p->id_asi");?>">EDITAR</a>
                  <a class="btn btn-danger" style="margin:0.1rem" href="<?php echo site_url("/Asistencias/eliminar/$p->id_asi");?>" >ELIMINAR</a>
                </td>
              </tr>
            <?php endforeach; ?>
            </tbody>
          </table>
        <?php else: ?>
          <h3>No Asistencias in database</h3>
        <?php endif; ?>
      </div>

    </div>

  </div>
</div>

<script type="text/javascript">
new DataTable('#tproduc', {
  layout: {
      topStart: {
          buttons: [
              {
                  extend: 'pdf',
                  text: 'Reporte PDF',
              },
              {
                  extend: 'print',
                  text: 'Reporte Imprimir',
              },
              {
                  extend: 'excel',
                  text: 'Reporte Excel',
              },
              {
                  extend: 'csv', // Cambiar a 'csv' para generar un archivo CSV
                  text: 'Reporte CSV', // Cambiar el texto del botón
              }
          ]
      }
  }
});
</script>

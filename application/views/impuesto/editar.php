<div class="content">
  <!-- Form Start -->
  <div class="container-fluid pt-4 px-4">
      <div class="row g-4">
          <div class="col-sm-12 col-xl-6">
              <div class="bg-secondary rounded h-100 p-4">
                  <h6 class="mb-4">Editar Impuesto</h6>
                  <form action="<?php echo site_url('Impuestos/procesarActualizacion');?>" method="post" role="form" enctype="multipart/form-data">
                      <div class="mb-3">
                          <label for="exampleInputEmail1" class="form-label">Nombre</label>
                          <input class="form-control" type="text" name="id_imp" id="id_imp" value="<?php echo $productEdit->id_imp?>" hidden>
                          <input class="form-control" type="text" name="nombre_imp" id="nombre_imp" value="<?php echo $productEdit->nombre_imp?>" required>
                      </div>
                      <div class="mb-3">
                          <label for="exampleInputEmail1" class="form-label">Descripción</label>
                          <input class="form-control" type="text" name="descripcion_imp" id="descripcion_imp" value="<?php echo $productEdit->descripcion_imp?>" required>
                      </div>
                      <div class="mb-3">
                          <label for="exampleInputEmail1" class="form-label">Porcentaje</label>
                          <input class="form-control" type="number" name="porcentaje_imp" id="porcentaje_imp" value="<?php echo $productEdit->porcentaje_imp?>" required>
                      </div>
                      <br>
                      <div class="form-floating mb-3">
                          <select class="form-select" id="estado_imp" required>
                              <option value="ACTIVO">Activo</option>
                              <option value="INACTIVO">Inactivo</option>
                          </select>
                          <label for="floatingSelect">Elija un Estado</label>
                      </div>
                      <button type="submit" class="btn btn-primary">Actualizar</button>
                      <a href="<?php echo site_url()?>/Impuestos/index" type="submit" class="btn btn-danger">Cancelar</a>
                  </form>
              </div>
          </div>

      </div>
  </div>
  <!-- Form End -->
</div>

<div class="content">

  <div class="container-fluid">
    <div class="row">

      <form class="form-control " id="form_nuevo" action="<?php echo site_url('Lecturas/guardar');?>" method="post" enctype="multipart/form-data">
        <br>
        <h2 class="text-center">NUEVA LECTURA</h2>

        <br>
        <div class="row">
          <div class="col-md-6">
        <label for="anio_lec">AÑO:</label>
        <br>
        <input type="number" class="form-control" name="anio_lec" id="anio_lec" placeholder="Ingrese año" required>
        <span id="error_anio" style="color: red;"></span>
    </div>
    <div class="col-md-6">
      <label for="">MES:</label>
      <select class="form-control"  name="mes_lec" id="mes_lec" required>
        <option value="ENERO">ENERO</option>
        <option value="FEBRERO">FEBRERO</option>
        <option value="MARZO">MARZO</option>
        <option value="ABRIL">ABRIL</option>
        <option value="MAYO">MAYO</option>
        <option value="JUNIO">JUNIO</option>
        <option value="JULIO">JULIO</option>
        <option value="AGOSTO">AGOSTO</option>
        <option value="SEPTIEMBRE">SEPTIEMBRE</option>
        <option value="OCTUBRE">OCTUBRE</option>
        <option value="NOVIEMBRE">NOVIEMBRE</option>
        <option value="DICIEMBRE">DICIEMBRE</option>
      </select>
    </div>

        </div>
        <br>
        <div class="row">
          <div class="col-md-4">
            <label for="">ESTADO:</label>
            <br>
            <select class="form-control"  name="estado_lec" id="estado_lec" required>
              <option value="COBRADA">COBRADA</option>
              <option value="INGRESADA">INGRESADA</option>
            </select>
          </div>
          <div class="col-md-4">
            <label for="">LECTURA ANTERIOR:</label>
            <br>
            <input type="number" class="form-control" name="lectura_anterior_lec" id="lectura_anterior_lec" placeholder="Ingrese valor" required>
          </div>
          <div class="col-md-4">
            <label for="">LECTURA ACTUAL:</label>
            <br>
            <input type="number" class="form-control" name="lectura_actual_lec" id="lectura_actual_lec" placeholder="Ingrese valor" required>
          </div>
        </div>
        <br>
        <div class="row">

          <div class="col-md-6">
            <label for="">HISTORIAL:</label>
            <select class="form-control" name="fk_id_his" id="fk_id_his" required>
                  <?php foreach ($historiales as $opcionTemporal){ ?>
                  <option value="<?php echo $opcionTemporal->id_his?>"> <?php echo $opcionTemporal->id_his?> </option>
                <?php };?>
            </select>
          </div>
          <div class="col-md-6">
            <label for="">CONSUMO:</label>
            <br>
            <select class="form-control" name="fk_id_consumo" id="fk_id_consumo" required>
                  <?php foreach ($consumos as $opcionTemporal){ ?>
                  <option value="<?php echo $opcionTemporal->id_consumo?>"> <?php echo $opcionTemporal->id_consumo?> </option>
                <?php };?>
            </select>
          </div>
        </div>

        <br>
        <div class="row">
          <button type="submit" class="btn btn-info col-md-5" style="margin:1rem">GUARDAR</button>
          <a href="<?php echo site_url()?>/Lecturas/index" class="btn btn-danger col-md-5" style="margin:1rem" >CANCELAR</a>
        </div>
      </form>
    </div>
  </div>

  <script>
      document.getElementById('anio_lec').addEventListener('input', function() {
          var anio = parseInt(this.value);
          var errorMensaje = document.getElementById('error_anio');
          if (isNaN(anio)) {
              errorMensaje.textContent = "Por favor ingrese un número válido.";
          } else if (anio < 2023 || anio > 2025) {
              errorMensaje.textContent = "Por favor ingrese una fecha de vencimiento entre 2023 y 2025.";
          } else {
              errorMensaje.textContent = "";
          }
      });
  </script>

</div>

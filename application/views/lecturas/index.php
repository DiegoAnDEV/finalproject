<div class="content">
  <div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
          <legend>LISTA DE LECTURAS</legend>
          <br>
          <div class="row">
            <div class="col-md-1 ">
              <a class="btn btn-secondary"   href="<?php echo site_url("Lecturas/nuevo");?>">NUEVO</a>
            </div>
          </div>
          <br>
          <?php if($listLec): ?>
            <table id="tdetalle" class="table table-striped">
              <thead>
                <tr class="table table-primary">
                  <td>ID</td>
                  <td>AÑO</td>
                  <td>MES</td>
                  <td>ESTADO</td>
                  <td>LECTURA ANTERIOR</td>
                  <td>LECTURA ACTUAL</td>
                  <td>HISTORIAL</td>
                  <td>CONSUMO</td>
                  <td >ACCIONES</td>
                </tr>
              </thead>
              <tbody>
              <?php foreach ($listLec as $p):?>
                <tr>
                  <td><?php echo $p->id_lec?></td>
                  <td><?php echo $p->anio_lec?></td>
                  <td><?php echo $p->mes_lec?></td>
                  <td><?php echo $p->estado_lec?></td>
                  <td><?php echo $p->lectura_anterior_lec?></td>
                  <td><?php echo $p->lectura_actual_lec?></td>
                  <td><?php echo $p->fk_id_his?></td>
                  <td><?php echo $p->fk_id_consumo?></td>
                  <td>
                    <a class="btn btn-info" style="margin:0.1rem"  href="<?php echo site_url("/Lecturas/editar/$p->id_lec");?>">EDITAR</a>
                    <a class="btn btn-danger" style="margin:0.1rem" href="<?php echo site_url("/Lecturas/eliminar/$p->id_lec");?>" >ELIMINAR</a>
                  </td>
                </tr>
              <?php endforeach; ?>
              </tbody>
            </table>
          <?php else: ?>
            <h3>No Asistencias in database</h3>
          <?php endif; ?>
        </div>

      </div>
      <script type="text/javascript">
      new DataTable('#tdetalle', {
        layout: {
            topStart: {
                buttons: [
                    {
                        extend: 'pdf',
                        text: 'Reporte PDF',
                    },
                    {
                        extend: 'print',
                        text: 'Reporte Imprimir',
                    },
                    {
                        extend: 'excel',
                        text: 'Reporte Excel',
                    },
                    {
                        extend: 'csv', // Cambiar a 'csv' para generar un archivo CSV
                        text: 'Reporte CSV', // Cambiar el texto del botón
                    }
                ]
            }
        }
      });
      </script>


    </div>
  </div>

</div>

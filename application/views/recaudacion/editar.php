<div class="content">
  <!-- Form Start -->
  <div class="container-fluid pt-4 px-4">
      <div class="row g-4">
          <div class="col-sm-12 col-xl-6">
              <div class="bg-secondary rounded h-100 p-4">
                  <h6 class="mb-4">Editar Recaudación</h6>
                  <form action="<?php echo site_url('Recaudaciones/procesarActualizacion');?>" method="post" role="form" enctype="multipart/form-data">
                    <div class="form-floating mb-3">
                      <select class="form-select" name="fk_id_soc" id="fk_id_soc">
                            <?php foreach ($socios as $opcionTemporal){ ?>
                            <option value="<?php echo $opcionTemporal->id_soc?>"> <?php echo $opcionTemporal->nombres_soc?> <?php echo $opcionTemporal->primer_apellido_soc?> </option>
                            <?php } ?>
                      </select>
                        <label for="floatingSelect">Elija un Socio</label>
                    </div>
                    <div class="form-floating mb-3">
                      <select class="form-select" name="estado_rec" id="fk_id_soc">
                            <option value="ABIERTO">Abierto</option>
                            <option value="CERRADO">Cerrado</option>
                      </select>
                        <label for="floatingSelect">Estado de la Recaudación</label>
                    </div>
                      <div class="mb-3">
                          <label for="exampleInputEmail1" class="form-label">Fecha de Emisión</label>
                          <input type="date" class="form-control" id="fecha_emision_rec" value="<?php echo $productEdit->fecha_emision_rec?>" required>
                          <input type="date" class="form-control" id="id_rec" value="<?php echo $productEdit->id_rec?>" hidden>
                      </div>

                      <button type="submit" class="btn btn-primary">Guardar</button>
                      <a href="<?php echo site_url()?>/Recaudaciones/index" type="submit" class="btn btn-danger">Cancelar</a>
                  </form>
              </div>
          </div>

      </div>
  </div>
  <!-- Form End -->
</div>

<div class="content">
  <div class="container-fluid">
    <div class="row">

      <div class="col-md-12">
        <legend>Lista de Rutas</legend>
        <a class="btn btn-success" href="<?php echo site_url('rutas/nuevo');?>">Nueva Ruta</a>
        <?php if ($cinemaList):?>
          <table class="table table-hover table-dark" id="tblProd" name="tblProd">
            <thead>
              <tr>
                <th>ID</th>
                <th>Nombre</th>
                <th>Descripcion</th>
                <th>Estado</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($cinemaList as $p): ?>
                <tr>
                  <td><?php echo $p->id_rut?></td>
                  <td><?php echo $p->nombre_rut?></td>
                  <td><?php echo $p->descripcion_rut?></td>
                  <td><?php echo $p->estado_rut?></td>
                  <td class="text-center">
                    <div class="">
                      <a class="btn btn-success" style="color: white;font-size: 16px;" href="<?php echo site_url();?>/rutas/editar/<?php echo $p->id_rut;?>" title="Edit Cinema">
                        Editar
                      </a>
                      &nbsp;&nbsp;&nbsp;&nbsp;
                      <a class="btn btn-danger" style="color:red;font-size: 16px; color:white"  href="<?php echo site_url();?>/rutas/eliminar/<?php echo $p->id_rut;?>" title="Delete ">
                        Eliminar
                      </a>
                    </div>
                  </td>
                </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
        <?php else: ?>
          <h3>No taxes in the Database</h3>
        <?php endif; ?>
      </div>
</div>




<script type="text/javascript">
new DataTable('#tblProd', {
  layout: {
      topStart: {
          buttons: [
              {
                  extend: 'pdf',
                  text: 'Reporte PDF',
              },
              {
                  extend: 'print',
                  text: 'Reporte Imprimir',
              },
              {
                  extend: 'excel',
                  text: 'Reporte Excel',
              },
              {
                  extend: 'csv', // Cambiar a 'csv' para generar un archivo CSV
                  text: 'Reporte CSV', // Cambiar el texto del botón
              }
          ]
      }
  }
});
</script>
<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>

<div class="content">
  <form class="form-control col-md-8" action="<?php echo site_url('Rutas/procesarActualizacion');?>" method="post" role="form" enctype="multipart/form-data">
    <div class="col-md-3" style="padding:1rem;">
      <label for="">Nombre:</label>
      <input class="form-control" type="text" name="id_rut" id="id_rut" value="<?php echo $rutaEdit->id_rut?>" hidden>
      <input class="form-control" type="text" name="nombre_rut" id="nombre_rut" value="<?php echo $rutaEdit->nombre_rut?>">
    </div>
    <div class="col-md-3" style="padding:1rem;">
      <label for="">Descripcion:</label>
      <input class="form-control" type="text" name="descripcion_rut" id="descripcion_rut" value="<?php echo $rutaEdit->descripcion_rut?>">
    </div>
    <div class="col-md-3" style="padding:1rem;">
      <label for="">Estado:</label>
      <select class="form-control" name="estado_rut" id="estado_rut">
        <option value="ACTIVO" <?php if($rutaEdit->estado_rut == "ACTIVO") echo "selected"; ?>>ACTIVO</option>
        <option value="INACTIVO" <?php if($rutaEdit->estado_rut == "INACTIVO") echo "selected"; ?>>INACTIVO</option>
      </select>
    </div>
    <div class="col-4 mx-auto">
      <button type="submit" class="btn btn-info form-control">SAVE</button>
    </div>
    <div class="col-4 mx-auto">
      <a href="<?php echo site_url()?>/rutas/index" type="submit" class="btn btn-danger form-control">CANCEL</a>
    </div>
  </form>
</div>

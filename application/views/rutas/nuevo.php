<div class="content">
  <form class="form-control col-md-8" action="<?php echo site_url('rutas/guardar');?>" method="post" role="form" enctype="multipart/form-data">
    <div class="col-md-3" style="padding:1rem;">
      <label for="nombre_rut">Nombre:</label>
      <input class="form-control" type="text" name="nombre_rut" id="nombre_rut" required>
    </div>
    <div class="col-md-3" style="padding:1rem;">
      <label for="descripcion_rut">Descripcion:</label>
      <input class="form-control" type="text" name="descripcion_rut" id="descripcion_rut" required>
    </div>
    <div class="col-md-3" style="padding:1rem;">
      <label for="estado_rut">Estado:</label>
      <select class="form-control" name="estado_rut" id="estado_rut" required>
        <option value="" disabled selected>Selecciona un estado</option>
        <option value="ACTIVO">ACTIVO</option>
        <option value="INACTIVO">INACTIVO</option>
      </select>
    </div>
    <div class="col-4 mx-auto">
        <button type="submit" class="btn btn-info form-control">Guardar</button>
    </div>
    <div class="col-4 mx-auto">
        <a href="<?php echo site_url()?>/rutas/index" type="submit" class="btn btn-danger form-control  text-dark">Cancelar</a>
    </div>
  </form>
</div>

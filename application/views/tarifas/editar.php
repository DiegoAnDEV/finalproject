<div class="content">
  <form class="form-control col-md-8" action="<?php echo site_url('Tarifas/procesarActualizacion');?>" method="post" role="form" enctype="multipart/form-data">
    <div class="col-md-3" style="padding:1rem;">
      <label for="">Nombre:</label>
      <input class="form-control" type="text" name="id_tar" id="id_ex" value="<?php echo $tarifaEdit->id_tar?>" hidden>
      <input class="form-control" type="text" name="limite_minimo_ex" id="limite_minimo_ex" value="<?php echo $tarifaEdit->nombre_tar?>">
    </div>
    <div class="col-md-3" style="padding:1rem;">
      <label for="">Descripcion:</label>
      <input class="form-control" type="text" name="descripcion_tar" id="descripcion_tar" value="<?php echo $tarifaEdit->descripcion_tar?>">
    </div>
    <div class="col-md-3" style="padding:1rem;">
      <label for="">Estado:</label>
      <select class="form-control" name="estado_tar" id="estado_tar">
        <option value="ACTIVO" <?php if($tarifaEdit->estado_tar == "ACTIVO") echo "selected"; ?>>ACTIVO</option>
        <option value="INACTIVO" <?php if($tarifaEdit->estado_tar == "INACTIVO") echo "selected"; ?>>INACTIVO</option>
      </select>
    </div>
    <div class="col-md-3" style="padding:1rem;">
      <label for="fecha_creacion_ex">Tarifa Maxima:</label>
      <input class="form-control" type="text" name="m3_maximo_tar" id="m3_maximo_tar" value="<?php echo $tarifaEdit->m3_maximo_tar?>">
    </div>
    <div class="col-md-3" style="padding:1rem;">
      <label for="fecha_actualizacion_ex">Tarifa Basica:</label>
      <input class="form-control" type="text" name="tarifa_basica_tar" id="tarifa_basica_tar" value="<?php echo $tarifaEdit->tarifa_basica_tar?>">
    </div>
    <div class="col-md-3" style="padding:1rem;">
      <label for="fecha_actualizacion_ex">Tarifa Excedente:</label>
      <input class="form-control" type="text" name="tarifa_excedente_tar" id="tarifa_excedente_tar" value="<?php echo $tarifaEdit->tarifa_excedente_tar?>">
    </div>
    <div class="col-md-3" style="padding:1rem;">
      <label for="fecha_actualizacion_ex">Valor:</label>
      <input class="form-control" type="text" name="valor_mora_tar" id="valor_mora_tar" value="<?php echo $tarifaEdit->valor_mora_tar?>">
    </div>
    <div class="col-4 mx-auto">
      <button type="submit" class="btn btn-info form-control">SAVE</button>
    </div>
    <div class="col-4 mx-auto">
      <a href="<?php echo site_url()?>/tarifas/index" type="submit" class="btn btn-danger form-control">CANCEL</a>
    </div>
  </form>
</div>

<div class="content">
  <form class="col-md-8" action="<?php echo site_url('Tarifas/guardar');?>" method="post" role="form" enctype="multipart/form-data">
    <div class="row">
      <div class="col-md-6">
        <div class="form-group">
          <label for="nombre_tar">Nombre:</label>
          <input class="form-control bg-light" type="text" name="nombre_tar" id="nombre_tar" required>
        </div>
        <div class="form-group">
          <label for="descripcion_tar">Descripción:</label>
          <input class="form-control bg-light" type="text" name="descripcion_tar" id="descripcion_tar" required>
        </div>
        <div class="form-group">
          <label for="estado_tar">Estado:</label>
          <select class="form-control bg-light" name="estado_tar" id="estado_tar" required>
            <option value="" disabled selected>Selecciona un estado</option>
            <option value="ACTIVO">ACTIVO</option>
            <option value="INACTIVO">INACTIVO</option>
          </select>
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
          <label for="m3_maximo_tar">Tarifa Máxima:</label>
          <input class="form-control bg-light" type="text" name="m3_maximo_tar" id="m3_maximo_tar" required>
        </div>
        <div class="form-group">
          <label for="tarifa_basica_tar">Tarifa Básica:</label>
          <input class="form-control bg-light" type="text" name="tarifa_basica_tar" id="tarifa_basica_tar" required>
        </div>
        <div class="form-group">
          <label for="tarifa_excedente_tar">Tarifa Excedente:</label>
          <input class="form-control bg-light" type="text" name="tarifa_excedente_tar" id="tarifa_excedente_tar" required>
        </div>
        <div class="form-group">
          <label for="valor_mora_tar">Valor:</label>
          <input class="form-control bg-light" type="text" name="valor_mora_tar" id="valor_mora_tar" required>
        </div>
      </div>
    </div>
    <div class="row mt-3">
      <div class="col-md-6">
        <button type="submit" class="btn btn-info form-control">GUARDAR</button>
      </div>
      <div class="col-md-6">
        <a href="<?php echo site_url()?>/tarifas/index" type="submit" class="btn btn-danger form-control text-dark">CANCELAR</a>
      </div>
    </div>
  </form>
</div>

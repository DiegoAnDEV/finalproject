<!DOCTYPE html>
<html lang="es" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Sistema de Dashboard YAKU</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="" name="keywords">
    <meta content="" name="description">

    <!-- Favicon -->
    <link href="<?php echo base_url();?>/plantilla/img/favicon.ico" rel="icon">

    <!-- Google Web Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;600&family=Roboto:wght@500;700&display=swap" rel="stylesheet">

    <!-- Icon Font Stylesheet -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.1/font/bootstrap-icons.css" rel="stylesheet">

    <!-- Libraries Stylesheet -->
    <link href="<?php echo base_url();?>plantilla/lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>plantilla/lib/tempusdominus/css/tempusdominus-bootstrap-4.min.css" rel="stylesheet" />

    <!-- Customized Bootstrap Stylesheet -->
    <link href="<?php echo base_url();?>plantilla/css/bootstrap.min.css" rel="stylesheet">

    <!-- Template Stylesheet -->
    <link href="<?php echo base_url();?>plantilla/css/style.css" rel="stylesheet">

    <!-- jQuery -->
    <script src="https://code.jquery.com/jquery-3.7.1.min.js" integrity="sha256-/JqT3SQfawRcv/BIHPThkBvs0OEvtFFmqPF/lYI/Cxo=" crossorigin="anonymous"></script>
    <!-- Datatables -->
    <link href="https://cdn.datatables.net/v/dt/jszip-3.10.1/dt-2.0.0/b-3.0.0/b-html5-3.0.0/b-print-3.0.0/datatables.min.css" rel="stylesheet">

<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.2.7/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.2.7/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/v/dt/jszip-3.10.1/dt-2.0.0/b-3.0.0/b-html5-3.0.0/b-print-3.0.0/datatables.min.js"></script>

  </head>
<body>


  <!-- Sidebar Start -->
  <div class="sidebar pe-4 pb-3">
      <nav class="navbar bg-secondary navbar-dark">
          <a href="index.html" class="navbar-brand mx-4 mb-3">
              <h3 class="text-primary"><i class="fa fa-user-edit me-2"></i>Sistema YAKU</h3>
          </a>
          <div class="d-flex align-items-center ms-4 mb-4">
              <div class="ms-3">
                  <h6 class="mb-0">Menú</h6>
              </div>
          </div>
          <div class="navbar-nav w-100">
              <a href="index.html" class="nav-item nav-link active"><i class="fa fa-tachometer-alt me-2"></i>Dashboard</a>
              <div class="nav-item dropdown">
                  <a href="#" class="nav-link dropdown-toggle" data-bs-toggle="dropdown"><i class="fa fa-laptop me-2"></i>Elements</a>
                  <div class="dropdown-menu bg-transparent border-0">
                      <a href="<?php echo site_url('Asistencias/index');?>" class="dropdown-item">Asistencias</a>
                      <a href="<?php echo site_url('Comunicados/index');?>" class="dropdown-item">Comunicados</a>
                      <a href="<?php echo site_url();?>/Configuraciones/index" class="dropdown-item">Configuración</a>
                      <a href="<?php echo site_url('Consumos/index');?>" class="dropdown-item">Consumos</a>
                      <a href="<?php echo site_url('Detalles/index');?>" class="dropdown-item">Detalles</a>
                      <a href="<?php echo site_url();?>/Eventos/index" class="dropdown-item">Eventos</a>
                      <a href="<?php echo site_url();?>/Exedentes/index" class="dropdown-item">Excedentes</a>
                      <a href="<?php echo site_url();?>/Historiales/index" class="dropdown-item">Historiales</a>
                      <a href="<?php echo site_url();?>/Impuestos/index" class="dropdown-item">Impuestos</a>
                      <a href="<?php echo site_url('Lecturas/index');?>" class="dropdown-item">Lecturas</a>
                      <a href="<?php echo site_url('Medidores/index');?>" class="dropdown-item">Medidores</a>
                      <a href="<?php echo site_url('Perfiles/index');?>" class="dropdown-item">Perfiles</a>
                      <a href="<?php echo site_url();?>/Recaudaciones/index" class="dropdown-item">Recaudaciones</a>
                      <a href="<?php echo site_url();?>/Rutas/index" class="dropdown-item">Rutas</a>
                      <a href="<?php echo site_url();?>/Tarifas/index" class="dropdown-item">Tarifa</a>
                      <a href="<?php echo site_url('TipoEventos/index');?>" class="dropdown-item">Tipo de Evento</a>
                      <a href="<?php echo site_url();?>/Usuarios/index" class="dropdown-item">Usuarios</a>
                  </div>
              </div>
          </div>
      </nav>
  </div>
  <!-- Sidebar End -->

<div class="content">
  <div class="container-fluid">
    <div class="row">

      <form class="form-control " id="form_nuevo" action="<?php echo site_url('Detalles/guardar');?>" method="post" enctype="multipart/form-data">
        <br>
        <h2 class="text-center">NUEVO DETALLE</h2>

        <br>
        <div class="row">
          <div class="col-md-6">
            <label for="">DETALLE:</label>
            <br>
            <input type="text" class="form-control" name="detalle_det" id="detalle_det" placeholder="Ingrese detalle" required>

          </div>
          <div class="col-md-6">
            <label for="">CANTIDAD:</label>
            <br>
            <input type="number" class="form-control" name="cantidad_det" id="cantidad_det" placeholder="Ingrese valor" required>
          </div>

        </div>
        <br>
        <div class="row">

          <div class="col-md-6">
            <label for="">LECTURA:</label>
            <select class="form-control" name="fk_id_lec" id="fk_id_lec" required>
                  <?php foreach ($lecturas as $opcionTemporal){ ?>
                  <option value="<?php echo $opcionTemporal->id_lec?>"> <?php echo $opcionTemporal->id_lec?> </option>
                <?php };?>
            </select>
          </div>
          <div class="col-md-6">
            <label for="">RECAUDACIÓN:</label>
            <br>
            <select class="form-control" name="fk_id_soc" id="fk_id_soc" required>
                  <?php foreach ($recaudaciones as $opcionTemporal){ ?>
                  <option value="<?php echo $opcionTemporal->id_rec?>"> <?php echo $opcionTemporal->id_rec?> </option>
                <?php };?>
            </select>
          </div>
        </div>
        <br>
        <div class="row">
          <div class="col-md-6">
            <label for="">IVA:</label>
            <br>
            <input type="number" class="form-control" name="iva_det" id="iva_det" placeholder="Ingrese valor" required>
          </div>
          <div class="col-md-6">
            <label for="">VALOR UNITARIO:</label>
            <br>
            <input type="number" class="form-control" name="valor_unitario_det" id="valor_unitario_det" placeholder="Ingrese valor" required>
          </div>

        </div>
        <br>
        <div class="row">
            <div class="col-md-6">
              <label for="">SUBTOTAL</label>
              <input type="number"  class="form-control" name="subtotal_det"  id="subtotal_det" placeholder="Ingrese valor" required>
            </div>

        </div>
        <br>
        <div class="row">
          <button type="submit" class="btn btn-info col-md-5" style="margin:1rem">GUARDAR</button>
          <a href="<?php echo site_url()?>/Detalles/index" class="btn btn-danger col-md-5" style="margin:1rem" >CANCELAR</a>
        </div>
      </form>
    </div>
  </div>

</div>

<div class="content">
  <div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
          <legend>LISTA DE DETALLES</legend>
          <br>
          <div class="row">
            <div class="col-md-1 ">
              <a class="btn btn-secondary"   href="<?php echo site_url("Detalles/nuevo");?>">NUEVO</a>
            </div>
          </div>
          <br>
          <?php if($listDet): ?>
            <table id="tdetalle" class="table table-striped">
              <thead>
                <tr class="table table-primary">
                  <td>ID</td>
                  <td>DETALLE</td>
                  <td>CANTIDAD</td>
                  <td>LECTURA</td>
                  <td>RECAUDACIÓN</td>
                  <td>IVA</td>
                  <td>SUBTOTAL</td>
                  <td>VALOR UNITARIO</td>
                  <td >ACCIONES</td>
                </tr>
              </thead>
              <tbody>
              <?php foreach ($listDet as $p):?>
                <tr>
                  <td><?php echo $p->id_det?></td>
                  <td><?php echo $p->detalle_det?></td>
                  <td><?php echo $p->cantidad_det?></td>
                  <td><?php echo $p->fk_id_lec?></td>
                  <td><?php echo $p->fk_id_rec?></td>
                  <td><?php echo $p->iva_det?></td>
                  <td><?php echo $p->subtotal_det?></td>
                  <td><?php echo $p->valor_unitario_det?></td>
                  <td>
                    <a class="btn btn-info" style="margin:0.1rem"  href="<?php echo site_url("/Detalles/editar/$p->id_det");?>">EDITAR</a>
                    <a class="btn btn-danger" style="margin:0.1rem" href="<?php echo site_url("/Detalles/eliminar/$p->id_det");?>" >ELIMINAR</a>
                  </td>
                </tr>
              <?php endforeach; ?>
              </tbody>
            </table>
          <?php else: ?>
            <h3>No Asistencias in database</h3>
          <?php endif; ?>
        </div>

      </div>

      <script type="text/javascript">
      new DataTable('#tdetalle', {
        layout: {
            topStart: {
                buttons: [
                    {
                        extend: 'pdf',
                        text: 'Reporte PDF',
                    },
                    {
                        extend: 'print',
                        text: 'Reporte Imprimir',
                    },
                    {
                        extend: 'excel',
                        text: 'Reporte Excel',
                    },
                    {
                        extend: 'csv', // Cambiar a 'csv' para generar un archivo CSV
                        text: 'Reporte CSV', // Cambiar el texto del botón
                    }
                ]
            }
        }
      });
      </script>

    </div>
  </div>

</div>

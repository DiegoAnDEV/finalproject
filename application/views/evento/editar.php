<div class="content">
  <!-- Form Start -->
  <div class="container-fluid pt-4 px-4">
      <div class="row g-4">
          <div class="col-sm-12 col-xl-6">
              <div class="bg-secondary rounded h-100 p-4">
                  <h6 class="mb-4">Editar Evento</h6>
                  <form action="<?php echo site_url('Eventos/procesarActualizacion');?>" method="post" role="form" enctype="multipart/form-data">
                      <div class="mb-3">
                          <label for="exampleInputEmail1" class="form-label">Descripcion</label>
                          <input type="text" class="form-control" id="id_eve" value="<?php echo $productEdit->id_eve?>" hidden>
                          <input type="text" class="form-control" id="descripcion_eve" value="<?php echo $productEdit->descripcion_eve?>" required>
                      </div>
                      <div class="mb-3">
                          <label for="exampleInputEmail1" class="form-label">Fecha Hora</label>
                          <input type="datetime" class="form-control" id="fecha_hora_eve" value="<?php echo $productEdit->fecha_hora_eve?>" required>
                      </div>
                      <div class="mb-3">
                          <label for="exampleInputEmail1" class="form-label">Lugar</label>
                          <input type="text" class="form-control" id="lugar_eve" value="<?php echo $productEdit->lugar_eve?>" required>
                      </div>
                      <div class="form-floating mb-3">
                        <select class="form-select" name="fk_id_te" id="fk_id_te">
                              <?php foreach ($tipos as $opcionTemporal){ ?>
                              <option value="<?php echo $opcionTemporal->id_te?>"> <?php echo $opcionTemporal->nombre_te?> </option>
                              <?php } ?>
                        </select>
                          <label for="floatingSelect">Pick a Type of Event</label>
                      </div>
                      <button type="submit" class="btn btn-primary">Save</button>
                      <a href="<?php echo site_url()?>/Eventos/index" type="submit" class="btn btn-danger">Cancel</a>
                  </form>
              </div>
          </div>

      </div>
  </div>
  <!-- Form End -->
</div>

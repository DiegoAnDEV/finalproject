<div class="row">
  <h3>Edit Product</h3>
  <form class="form-control col-md-8" action="<?php echo site_url('Products/procesarActualizacion');?>" method="post" role="form" enctype="multipart/form-data">
    <div class="col-md-3" style="padding:1rem;">
      <input type="text" name="id_pro" id="id_pro" value="<?php echo $productEdit->id_pro?>" hidden>
      <label for="">Name:</label>
      <input class="form-control" type="text" name="name_pro" id="name_pro" value="<?php echo $productEdit->name_pro?>">
    </div>
    <div class="col-md-3" style="padding:1rem;">
      <label for="">Category:</label>
      <select class="form-control" name="category_pro" id="category_pro">
        <option value="">---Pick a category---</option>
        <option value="OILS">OILS</option>
        <option value="FRUITS">FRUITS</option>
        <option value="SNACKS">SNACKS</option>
        <option value="VEGETABLES">VEGETABLES</option>
        <option value="LIQUORS">LIQUORS</option>
      </select>
    </div>
    <div class="col-md-3" style="padding:1rem;">
      <label for="">IVA</label>
      <select class="form-control" name="iva_pro" id="iva_pro">
        <option value="">---Pick a value---</option>
        <option value=0>0%</option>
        <option value=12>12%</option>
      </select>
    </div>
    <div class="col-md-3" style="padding:1rem;">
      <label for="">Price:</label>
      <input class="form-control" type="number" name="price_pro" id="price_pro" value="<?php echo $productEdit->price_pro?>">
    </div>
    <div class="col-md-3" style="padding:1rem;">
      <label for="">Stock:</label>
      <input class="form-control" type="number" name="stock_pro" id="stock_pro" value="<?php echo $productEdit->stock_pro?>">
    </div>
    <div class="col-md-3" style="padding:1rem;">
      <label for="">Country:</label>
      <input class="form-control" type="text" name="country_pro" id="country_pro" value="<?php echo $productEdit->country_pro?>">
    </div>
  </div>
  <div class="row">
    <div class="col-4 mx-auto">
        <button type="submit" class="btn btn-info form-control">SAVE</button>
    </div>
    <div class="col-4 mx-auto">
        <!-- <button type="submit" class="form-control">Cancelar</button> -->
        <a href="<?php echo site_url()?>/Products/index" type="submit" class="btn btn-danger form-control" style="color:white">CANCEL</a>
    </div>
  </div>
</form>

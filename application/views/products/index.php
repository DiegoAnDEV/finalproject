<div class="container-fluid">
  <div class="row">

    <div class="col-md-6">
      <legend>LIST OF CINEMAS</legend>
      <a class="btn btn-success" href="<?php echo site_url('Products/nuevo');?>">New Product</a>
      <?php if ($cinemaList):?>
        <table class="table table-hover table-dark" id="tblProd" name="tblProd">
          <thead>
            <tr>
              <th>ID</th>
              <th>NAME</th>
              <th>CITY</th>
              <th>PROVINCE</th>
              <th>CAPACITY</th>
              <th>SNACK</th>
              <th>SCHEDULE</th>
              <th>TICKET COST</th>
              <th>LANGUAGES</th>
              <th>RESTRICTED MOVIES</th>
              <th>INDIE MOVIES</th>
              <th>ACTIONS</th>
            </tr>
          </thead>
          <tbody>
            <?php foreach ($cinemaList as $p): ?>
              <tr>
                <td><?php echo $p->id_cin?></td>
                <td><?php echo $p->name_cin?></td>
                <td><?php echo $p->city_cin?></td>
                <td><?php echo $p->province_cin?></td>
                <td><?php echo $p->capacity_cin?></td>
                <td><?php echo $p->snack_cin?></td>
                <td><?php echo $p->schedule_cin?></td>
                <td><?php echo $p->ticket_cin?></td>
                <td><?php echo $p->language_movies?></td>
                <td><?php echo $p->restricted_movies?></td>
                <td><?php echo $p->indie_movies?></td>
                <td class="text-center">
                  <div class="">
                    <a class="btn btn-success" style="color: white;font-size: 16px;" href="<?php echo site_url();?>/Products/editar/<?php echo $p->id_cin;?>" title="Edit Cinema">
                      Edit
            				</a>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <a class="btn btn-danger" style="color:red;font-size: 16px; color:white"  href="<?php echo site_url();?>/Products/eliminar/<?php echo $p->id_cin;?>" title="Delete ">
                      Delete
            				</a>
                  </div>
          			</td>
              </tr>
            <?php endforeach; ?>
          </tbody>
        </table>
      <?php else: ?>
        <h3>No products in the Database</h3>
      <?php endif; ?>
    </div>

    </div>
    <h2>DASHBOARDS</h2>
    <div class="row">
      <div class="col-md-6">
        <div class="card">
          <div class="card-body">
            <canvas id="totalcity" width="600" height="300"></canvas>
              <h5 class="card-title">Total Cinemas by City</h5>
          </div>
        </div>
      </div>

      <div class="col-md-4">
        <div class="card">
          <div class="card-body">
            <canvas id="percity" width="400" height="300"></canvas>
              <h5 class="card-title">Percentage Cinemas by City</h5>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-6">
        <div class="card">
          <div class="card-body">
            <canvas id="totalpro" width="600" height="300"></canvas>
              <h5 class="card-title">Total Cinemas by Province</h5>
          </div>
        </div>
      </div>

      <div class="col-md-4">
        <div class="card">
          <div class="card-body">
            <canvas id="perpro" width="600" height="300"></canvas>
              <h5 class="card-title">Percentage Cinemas by Province</h5>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-6">
        <div class="card">
          <div class="card-body">
            <canvas id="totalcap" width="600" height="300"></canvas>
              <h5 class="card-title">Total Cinemas by Capacity</h5>
          </div>
        </div>
      </div>

      <div class="col-md-4">
        <div class="card">
          <div class="card-body">
            <canvas id="percap" width="600" height="300"></canvas>
              <h5 class="card-title">Percentage Cinemas by Capacity</h5>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-6">
        <div class="card">
          <div class="card-body">
            <canvas id="totalsnack" width="600" height="300"></canvas>
              <h5 class="card-title">Total Cinemas by Snacks</h5>
          </div>
        </div>
      </div>

      <div class="col-md-4">
        <div class="card">
          <div class="card-body">
            <canvas id="persnack" width="600" height="300"></canvas>
              <h5 class="card-title">Percentage Cinemas by Snacks</h5>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-6">
        <div class="card">
          <div class="card-body">
            <canvas id="totallan" width="600" height="300"></canvas>
              <h5 class="card-title">Total Cinemas by Languages</h5>
          </div>
        </div>
      </div>

      <div class="col-md-4">
        <div class="card">
          <div class="card-body">
            <canvas id="perlan" width="600" height="300"></canvas>
              <h5 class="card-title">Percentage Cinemas by Languages</h5>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-6">
        <div class="card">
          <div class="card-body">
            <canvas id="totalres" width="600" height="300"></canvas>
              <h5 class="card-title">Total Cinemas by Restriction</h5>
          </div>
        </div>
      </div>

      <div class="col-md-4">
        <div class="card">
          <div class="card-body">
            <canvas id="perres" width="600" height="300"></canvas>
              <h5 class="card-title">Percentage Cinemas by Restriction</h5>
          </div>
        </div>
      </div>
    </div>


  </div>
</div>


<script type="text/javascript">
  $("#tblProd").DataTable();
</script>
<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>

<script>
    // Datos de ejemplo
    var datos = {
      labels: [
        <?php if ($cinemaCity):?>
          <?php foreach ($cinemaCity as $product):?>
            '<?php echo $product->city_cin;?>',
          <?php endforeach; ?>
        <?php endif; ?>
      ],
      datasets: [{
        label: 'Total',
        data: [
          <?php if ($cinemaCity):?>
            <?php foreach ($cinemaCity as $product):?>
              <?php echo $product->total_city;?>,
            <?php endforeach; ?>
          <?php endif; ?>
        ], // Valores de las barras
        backgroundColor: [
          'rgba(255, 99, 132, 0.6)', // Color de la primera barra
          'rgba(54, 162, 235, 0.6)', // Color de la segunda barra
          'rgba(255, 206, 86, 0.6)'  // Color de la tercera barra
        ],
        borderColor: [
          'rgba(255, 99, 132, 1)',
          'rgba(54, 162, 235, 1)',
          'rgba(255, 206, 86, 1)'
        ],
        borderWidth: 1
      }]
    };
    // Opciones de configuración
    var opciones = {
      scales: {
        y: {
          beginAtZero: true
        }
      }
    };
    // Obtener el contexto del lienzo
    var contexto = document.getElementById('totalcity').getContext('2d');
    // Crear el gráfico de barras
    var graficoDeBarras = new Chart(contexto, {
      type: 'bar',
      data: datos,
      options: opciones
    });
  </script>
<!-- Porcentaje city -->
  <script>
      // Datos de ejemplo
      var datos = {
        labels: [
          <?php if ($cinemaCity):?>
            <?php foreach ($cinemaCity as $product):?>
              '<?php echo $product->city_cin;?>',
            <?php endforeach; ?>
          <?php endif; ?>
        ],
        datasets: [{
          label: 'Total',
          data: [
            <?php if ($cinemaCity):?>
              <?php foreach ($cinemaCity as $product):?>
                <?php echo $product->percentage;?>,
              <?php endforeach; ?>
            <?php endif; ?>
          ], // Valores de las barras
          backgroundColor: [
            'rgba(255, 99, 132, 0.6)', // Color de la primera barra
            'rgba(54, 162, 235, 0.6)', // Color de la segunda barra
            'rgba(255, 206, 86, 0.6)'  // Color de la tercera barra
          ],
          borderColor: [
            'rgba(255, 99, 132, 1)',
            'rgba(54, 162, 235, 1)',
            'rgba(255, 206, 86, 1)'
          ],
          borderWidth: 1
        }]
      };
      // Opciones de configuración
      var opciones = {
        scales: {
          y: {
            beginAtZero: true
          }
        }
      };
      // Obtener el contexto del lienzo
      var contexto = document.getElementById('percity').getContext('2d');
      // Crear el gráfico de barras
      var graficoDeBarras = new Chart(contexto, {
        type: 'pie',
        data: datos,
        options: opciones
      });
    </script>
    <!-- Total proince -->
      <script>
          // Datos de ejemplo
          var datos = {
            labels: [
              <?php if ($cinemaProvince):?>
                <?php foreach ($cinemaProvince as $product):?>
                  '<?php echo $product->province_cin;?>',
                <?php endforeach; ?>
              <?php endif; ?>
            ],
            datasets: [{
              label: 'Total',
              data: [
                <?php if ($cinemaProvince):?>
                  <?php foreach ($cinemaProvince as $product):?>
                    <?php echo $product->total_province;?>,
                  <?php endforeach; ?>
                <?php endif; ?>
              ], // Valores de las barras
              backgroundColor: [
                'rgba(255, 99, 132, 0.6)', // Color de la primera barra
                'rgba(54, 162, 235, 0.6)', // Color de la segunda barra
                'rgba(255, 206, 86, 0.6)'  // Color de la tercera barra
              ],
              borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)'
              ],
              borderWidth: 1
            }]
          };
          // Opciones de configuración
          var opciones = {
            scales: {
              y: {
                beginAtZero: true
              }
            }
          };
          // Obtener el contexto del lienzo
          var contexto = document.getElementById('totalpro').getContext('2d');
          // Crear el gráfico de barras
          var graficoDeBarras = new Chart(contexto, {
            type: 'line',
            data: datos,
            options: opciones
          });
        </script>

        <!-- Percentage proince -->
          <script>
              // Datos de ejemplo
              var datos = {
                labels: [
                  <?php if ($cinemaProvince):?>
                    <?php foreach ($cinemaProvince as $product):?>
                      '<?php echo $product->province_cin;?>',
                    <?php endforeach; ?>
                  <?php endif; ?>
                ],
                datasets: [{
                  label: 'Total',
                  data: [
                    <?php if ($cinemaProvince):?>
                      <?php foreach ($cinemaProvince as $product):?>
                        <?php echo $product->percentage;?>,
                      <?php endforeach; ?>
                    <?php endif; ?>
                  ], // Valores de las barras
                  backgroundColor: [
                    'rgba(255, 99, 132, 0.6)', // Color de la primera barra
                    'rgba(54, 162, 235, 0.6)', // Color de la segunda barra
                    'rgba(255, 206, 86, 0.6)'  // Color de la tercera barra
                  ],
                  borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)'
                  ],
                  borderWidth: 1
                }]
              };
              // Opciones de configuración
              var opciones = {
                scales: {
                  y: {
                    beginAtZero: true
                  }
                }
              };
              // Obtener el contexto del lienzo
              var contexto = document.getElementById('perpro').getContext('2d');
              // Crear el gráfico de barras
              var graficoDeBarras = new Chart(contexto, {
                type: 'doughnut',
                data: datos,
                options: opciones
              });
            </script>

            <!-- Total capcity -->
              <script>
                  // Datos de ejemplo
                  var datos = {
                    labels: [
                      <?php if ($cinemaCapacity):?>
                        <?php foreach ($cinemaCapacity as $product):?>
                          '<?php echo $product->capacity_cin;?>',
                        <?php endforeach; ?>
                      <?php endif; ?>
                    ],
                    datasets: [{
                      label: 'Total',
                      data: [
                        <?php if ($cinemaCapacity):?>
                          <?php foreach ($cinemaCapacity as $product):?>
                            <?php echo $product->total;?>,
                          <?php endforeach; ?>
                        <?php endif; ?>
                      ], // Valores de las barras
                      backgroundColor: [
                        'rgba(255, 99, 132, 0.6)', // Color de la primera barra
                        'rgba(54, 162, 235, 0.6)', // Color de la segunda barra
                        'rgba(255, 206, 86, 0.6)'  // Color de la tercera barra
                      ],
                      borderColor: [
                        'rgba(255, 99, 132, 1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)'
                      ],
                      borderWidth: 1
                    }]
                  };
                  // Opciones de configuración
                  var opciones = {
                    scales: {
                      y: {
                        beginAtZero: true
                      }
                    }
                  };
                  // Obtener el contexto del lienzo
                  var contexto = document.getElementById('totalcap').getContext('2d');
                  // Crear el gráfico de barras
                  var graficoDeBarras = new Chart(contexto, {
                    type: 'polarArea',
                    data: datos,
                    options: opciones
                  });
                </script>


<!-- Total capcity -->
  <script>
      // Datos de ejemplo
      var datos = {
        labels: [
          <?php if ($cinemaCapacity):?>
            <?php foreach ($cinemaCapacity as $product):?>
              '<?php echo $product->capacity_cin;?>',
            <?php endforeach; ?>
          <?php endif; ?>
        ],
        datasets: [{
          label: 'Total',
          data: [
            <?php if ($cinemaCapacity):?>
              <?php foreach ($cinemaCapacity as $product):?>
                <?php echo $product->percentage;?>,
              <?php endforeach; ?>
            <?php endif; ?>
          ], // Valores de las barras
          backgroundColor: [
            'rgba(255, 99, 132, 0.6)', // Color de la primera barra
            'rgba(54, 162, 235, 0.6)', // Color de la segunda barra
            'rgba(255, 206, 86, 0.6)'  // Color de la tercera barra
          ],
          borderColor: [
            'rgba(255, 99, 132, 1)',
            'rgba(54, 162, 235, 1)',
            'rgba(255, 206, 86, 1)'
          ],
          borderWidth: 1
        }]
      };
      // Opciones de configuración
      var opciones = {
        scales: {
          y: {
            beginAtZero: true
          }
        }
      };
      // Obtener el contexto del lienzo
      var contexto = document.getElementById('percap').getContext('2d');
      // Crear el gráfico de barras
      var graficoDeBarras = new Chart(contexto, {
        type: 'radar',
        data: datos,
        options: opciones
      });
    </script>


    <!-- Total capcity -->
      <script>
          // Datos de ejemplo
          var datos = {
            labels: [
              <?php if ($cinemaSnack):?>
                <?php foreach ($cinemaSnack as $product):?>
                  '<?php echo $product->snack_cin;?>',
                <?php endforeach; ?>
              <?php endif; ?>
            ],
            datasets: [{
              label: 'Total',
              data: [
                <?php if ($cinemaSnack):?>
                  <?php foreach ($cinemaSnack as $product):?>
                    <?php echo $product->total;?>,
                  <?php endforeach; ?>
                <?php endif; ?>
              ], // Valores de las barras
              backgroundColor: [
                'rgba(255, 99, 132, 0.6)', // Color de la primera barra
                'rgba(54, 162, 235, 0.6)', // Color de la segunda barra
                'rgba(255, 206, 86, 0.6)'  // Color de la tercera barra
              ],
              borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)'
              ],
              borderWidth: 1
            }]
          };
          // Opciones de configuración
          var opciones = {
            scales: {
              y: {
                beginAtZero: true
              }
            }
          };
          // Obtener el contexto del lienzo
          var contexto = document.getElementById('totalsnack').getContext('2d');
          // Crear el gráfico de barras
          var graficoDeBarras = new Chart(contexto, {
            type: 'bar',
            data: datos,
            options: opciones
          });
        </script>


        <script>
            // Datos de ejemplo
            var datos = {
              labels: [
                <?php if ($cinemaSnack):?>
                  <?php foreach ($cinemaSnack as $product):?>
                    '<?php echo $product->snack_cin;?>',
                  <?php endforeach; ?>
                <?php endif; ?>
              ],
              datasets: [{
                label: 'Total',
                data: [
                  <?php if ($cinemaSnack):?>
                    <?php foreach ($cinemaSnack as $product):?>
                      <?php echo $product->percentage;?>,
                    <?php endforeach; ?>
                  <?php endif; ?>
                ], // Valores de las barras
                backgroundColor: [
                  'rgba(255, 99, 132, 0.6)', // Color de la primera barra
                  'rgba(54, 162, 235, 0.6)', // Color de la segunda barra
                  'rgba(255, 206, 86, 0.6)'  // Color de la tercera barra
                ],
                borderColor: [
                  'rgba(255, 99, 132, 1)',
                  'rgba(54, 162, 235, 1)',
                  'rgba(255, 206, 86, 1)'
                ],
                borderWidth: 1
              }]
            };
            // Opciones de configuración
            var opciones = {
              scales: {
                y: {
                  beginAtZero: true
                }
              }
            };
            // Obtener el contexto del lienzo
            var contexto = document.getElementById('persnack').getContext('2d');
            // Crear el gráfico de barras
            var graficoDeBarras = new Chart(contexto, {
              type: 'pie',
              data: datos,
              options: opciones
            });
          </script>

          <script>
              // Datos de ejemplo
              var datos = {
                labels: [
                  <?php if ($cinemaLan):?>
                    <?php foreach ($cinemaLan as $product):?>
                      '<?php echo $product->language_movies;?>',
                    <?php endforeach; ?>
                  <?php endif; ?>
                ],
                datasets: [{
                  label: 'Total',
                  data: [
                    <?php if ($cinemaLan):?>
                      <?php foreach ($cinemaLan as $product):?>
                        <?php echo $product->total;?>,
                      <?php endforeach; ?>
                    <?php endif; ?>
                  ], // Valores de las barras
                  backgroundColor: [
                    'rgba(255, 99, 132, 0.6)', // Color de la primera barra
                    'rgba(54, 162, 235, 0.6)', // Color de la segunda barra
                    'rgba(255, 206, 86, 0.6)'  // Color de la tercera barra
                  ],
                  borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)'
                  ],
                  borderWidth: 1
                }]
              };
              // Opciones de configuración
              var opciones = {
                scales: {
                  y: {
                    beginAtZero: true
                  }
                }
              };
              // Obtener el contexto del lienzo
              var contexto = document.getElementById('totallan').getContext('2d');
              // Crear el gráfico de barras
              var graficoDeBarras = new Chart(contexto, {
                type: 'line',
                data: datos,
                options: opciones
              });
            </script>


            <script>
                // Datos de ejemplo
                var datos = {
                  labels: [
                    <?php if ($cinemaLan):?>
                      <?php foreach ($cinemaLan as $product):?>
                        '<?php echo $product->language_movies;?>',
                      <?php endforeach; ?>
                    <?php endif; ?>
                  ],
                  datasets: [{
                    label: 'Total',
                    data: [
                      <?php if ($cinemaLan):?>
                        <?php foreach ($cinemaLan as $product):?>
                          <?php echo $product->percentage;?>,
                        <?php endforeach; ?>
                      <?php endif; ?>
                    ], // Valores de las barras
                    backgroundColor: [
                      'rgba(255, 99, 132, 0.6)', // Color de la primera barra
                      'rgba(54, 162, 235, 0.6)', // Color de la segunda barra
                      'rgba(255, 206, 86, 0.6)'  // Color de la tercera barra
                    ],
                    borderColor: [
                      'rgba(255, 99, 132, 1)',
                      'rgba(54, 162, 235, 1)',
                      'rgba(255, 206, 86, 1)'
                    ],
                    borderWidth: 1
                  }]
                };
                // Opciones de configuración
                var opciones = {
                  scales: {
                    y: {
                      beginAtZero: true
                    }
                  }
                };
                // Obtener el contexto del lienzo
                var contexto = document.getElementById('perlan').getContext('2d');
                // Crear el gráfico de barras
                var graficoDeBarras = new Chart(contexto, {
                  type: 'doughnut',
                  data: datos,
                  options: opciones
                });
              </script>

              <script>
                  // Datos de ejemplo
                  var datos = {
                    labels: [
                      <?php if ($cinemaRes):?>
                        <?php foreach ($cinemaRes as $product):?>
                          '<?php echo $product->restricted_movies;?>',
                        <?php endforeach; ?>
                      <?php endif; ?>
                    ],
                    datasets: [{
                      label: 'Total',
                      data: [
                        <?php if ($cinemaRes):?>
                          <?php foreach ($cinemaRes as $product):?>
                            <?php echo $product->total;?>,
                          <?php endforeach; ?>
                        <?php endif; ?>
                      ], // Valores de las barras
                      backgroundColor: [
                        'rgba(255, 99, 132, 0.6)', // Color de la primera barra
                        'rgba(54, 162, 235, 0.6)', // Color de la segunda barra
                        'rgba(255, 206, 86, 0.6)'  // Color de la tercera barra
                      ],
                      borderColor: [
                        'rgba(255, 99, 132, 1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)'
                      ],
                      borderWidth: 1
                    }]
                  };
                  // Opciones de configuración
                  var opciones = {
                    scales: {
                      y: {
                        beginAtZero: true
                      }
                    }
                  };
                  // Obtener el contexto del lienzo
                  var contexto = document.getElementById('totalres').getContext('2d');
                  // Crear el gráfico de barras
                  var graficoDeBarras = new Chart(contexto, {
                    type: 'polarArea',
                    data: datos,
                    options: opciones
                  });
                </script>

                <script>
                    // Datos de ejemplo
                    var datos = {
                      labels: [
                        <?php if ($cinemaRes):?>
                          <?php foreach ($cinemaRes as $product):?>
                            '<?php echo $product->restricted_movies;?>',
                          <?php endforeach; ?>
                        <?php endif; ?>
                      ],
                      datasets: [{
                        label: 'Total',
                        data: [
                          <?php if ($cinemaRes):?>
                            <?php foreach ($cinemaRes as $product):?>
                              <?php echo $product->percentage;?>,
                            <?php endforeach; ?>
                          <?php endif; ?>
                        ], // Valores de las barras
                        backgroundColor: [
                          'rgba(255, 99, 132, 0.6)', // Color de la primera barra
                          'rgba(54, 162, 235, 0.6)', // Color de la segunda barra
                          'rgba(255, 206, 86, 0.6)'  // Color de la tercera barra
                        ],
                        borderColor: [
                          'rgba(255, 99, 132, 1)',
                          'rgba(54, 162, 235, 1)',
                          'rgba(255, 206, 86, 1)'
                        ],
                        borderWidth: 1
                      }]
                    };
                    // Opciones de configuración
                    var opciones = {
                      scales: {
                        y: {
                          beginAtZero: true
                        }
                      }
                    };
                    // Obtener el contexto del lienzo
                    var contexto = document.getElementById('perres').getContext('2d');
                    // Crear el gráfico de barras
                    var graficoDeBarras = new Chart(contexto, {
                      type: 'doughnut',
                      data: datos,
                      options: opciones
                    });
                  </script>

<div class="content">
  <div class="container-fluid">
    <div class="row">

      <form class="form-control " id="form_nuevo" action="<?php echo site_url('TipoEventos/guardar');?>" method="post" enctype="multipart/form-data">
        <br>
        <h2 class="text-center">NUEVO TIPO</h2>

        <br>
        <div class="row">
          <div class="col-md-6">
            <label for="">NOMBRE:</label>
            <br>
            <input type="text" class="form-control" name="nombre_te" id="nombre_te" placeholder="Ingrese nombre" required>
          </div>
          <div class="col-md-6">
            <label for="">ESTADO:</label>
            <br>
            <select class="form-control"  name="estado_te" id="estado_te" required>
              <option value="ACTIVO">ACTIVO</option>
              <option value="INACTIVO">INACTIVO</option>
            </select>
          </div>

        </div>

        <br>

        <div class="row">
          <button type="submit" class="btn btn-info col-md-5" style="margin:1rem">GUARDAR</button>
          <a href="<?php echo site_url()?>/TipoEventos/index" class="btn btn-danger col-md-5" style="margin:1rem" >CANCELAR</a>
        </div>
      </form>
    </div>
  </div>

</div>

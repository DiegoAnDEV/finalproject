<div class="content">
    <div class="row">
      <div class="col-md-12">
        <legend>Usuarios</legend>
        <a class="btn btn-success" href="<?php echo site_url('Usuarios/nuevo');?>">Nuevo Usuario</a>
        <?php if ($cinemaList):?>
          <table class="table table-hover table-dark" id="tblProd" name="tblProd">
            <thead>
              <tr>
                <th>ID</th>
                <th>Nombre</th>
                <th>Apellido</th>
                <th>Email</th>
                <th>Contraseña</th>
                <th>Estado</th>
                <th>Perfil</th>
                <th>Acciones</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($cinemaList as $p): ?>
                <tr>
                  <td><?php echo $p->id_usu?></td>
                  <td><?php echo $p->nombre_usu?></td>
                  <td><?php echo $p->apellido_usu?></td>
                  <td><?php echo $p->email_usu?></td>
                  <td><?php echo $p->password_usu?></td>
                  <td><?php echo $p->estado_usu?></td>
                  <td><?php echo $p->fk_id_per?></td>
                  <td class="text-center">
                    <div class="">
                      <a class="btn btn-success" style="color: white;font-size: 16px;" href="<?php echo site_url();?>/Usuarios/editar/<?php echo $p->id_usu;?>" title="Edit">
                        Editar
                      </a>
                      &nbsp;&nbsp;&nbsp;&nbsp;
                      <a class="btn btn-danger" style="color:red;font-size: 16px; color:white"  href="<?php echo site_url();?>/Usuarios/eliminar/<?php echo $p->id_usu;?>" title="Delete ">
                        Borrar
                      </a>
                    </div>
                  </td>
                </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
        <?php else: ?>
          <h3>No info in the Database</h3>
        <?php endif; ?>
      </div>
</div>
</div>
<script type="text/javascript">
new DataTable('#tblProd', {
  layout: {
      topStart: {
          buttons: [
              {
                  extend: 'pdf',
                  text: 'Reporte PDF',
              },
              {
                  extend: 'print',
                  text: 'Reporte Imprimir',
              },
              {
                  extend: 'excel',
                  text: 'Reporte Excel',
              },
              {
                  extend: 'csv', // Cambiar a 'csv' para generar un archivo CSV
                  text: 'Reporte CSV', // Cambiar el texto del botón
              }
          ]
      }
  }
});
</script>
<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>

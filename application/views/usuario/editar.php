<div class="content">
  <!-- Form Start -->
  <div class="container-fluid pt-4 px-4">
      <div class="row g-4">
          <div class="col-sm-12 col-xl-6">
              <div class="bg-secondary rounded h-100 p-4">
                  <h6 class="mb-4">New User</h6>
                  <form action="<?php echo site_url('Usuarios/procesarActualizacion');?>" method="post" role="form" enctype="multipart/form-data">
                      <div class="mb-3">
                          <label for="exampleInputEmail1" class="form-label">Nombre</label>
                          <input type="text" class="form-control" id="id_usu" value="<?php echo $productEdit->id_usu?>" hidden>
                          <input type="text" class="form-control" id="nombre_usu" value="<?php echo $productEdit->nombre_usu?>" required>
                      </div>
                      <div class="mb-3">
                          <label for="exampleInputEmail1" class="form-label">Apellido</label>
                          <input type="text" class="form-control" id="apellido_usu" value="<?php echo $productEdit->apellido_usu?>" required>
                      </div>
                      <div class="mb-3">
                          <label for="exampleInputEmail1" class="form-label">Email</label>
                          <input type="text" class="form-control" id="email_usu" value="<?php echo $productEdit->email_usu?>" required>
                      </div>
                      <div class="mb-3">
                          <label for="exampleInputPassword1" class="form-label">Clave</label>
                          <input type="password" class="form-control" id="password_usu" value="<?php echo $productEdit->password_usu?>" required>
                      </div>
                      <br>
                      <div class="form-floating mb-3">
                          <select class="form-select" id="estado_usu" required>
                              <option value="ACTIVO">Activo</option>
                              <option value="INACTIVO">Inactivo</option>
                          </select>
                          <label for="floatingSelect">Elegir un Estado</label>
                      </div>
                      <br>
                      <div class="form-floating mb-3">
                          <select class="form-select" id="fk_id_per" required>
                              <option value="1">OPERADOR</option>
                              <option value="2">ADMINISTRADOR</option>
                          </select>
                          <label for="floatingSelect">Elegir un Perfil</label>
                      </div>

                      <button type="submit" class="btn btn-primary">Actualizar</button>
                      <a href="<?php echo site_url()?>/Usuarios/index" type="submit" class="btn btn-danger">Cancelar</a>
                  </form>
              </div>
          </div>

      </div>
  </div>
  <!-- Form End -->
</div>

<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <legend>Lista de Medidores</legend>
        <a class="btn btn-success" href="<?php echo site_url('medidores/nuevo');?>">Nuevo Medidor</a>
        <?php if ($cinemaList):?>
          <table class="table table-hover table-dark" id="tblProd" name="tblProd">
            <thead>
              <tr>
                <th>ID</th>
                <th>ID Ruta</th>
                <th>ID Tarifa</th>
                <th>Numero de Medidor</th>
                <th>Serie</th>
                <th>Marca Medidor</th>
                <th>Observacion</th>
                <th>Estado</th>
                <th>Foto Medidor</th>
                <th>Creacion Medidor</th>
                <th>Actualizacion Medidor</th>
                <th>Lectura Inicial</th>
                <th>Acciones</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($cinemaList as $p): ?>
                <tr>
                  <td><?php echo $p->id_med?></td>
                  <td><?php echo $p->fk_id_rut?></td>
                  <td><?php echo $p->fk_id_tar?></td>
                  <td><?php echo $p->numero_med?></td>
                  <td><?php echo $p->serie_med?></td>
                  <td><?php echo $p->marca_med?></td>
                  <td><?php echo $p->observacion_med?></td>
                  <td><?php echo $p->estado_med?></td>
                  <td><?php echo $p->foto_med?></td>
                  <td><?php echo $p->creacion_med?></td>
                  <td><?php echo $p->actualizacion_med?></td>
                  <td><?php echo $p->lectura_inicial_med?></td>
                  <td class="text-center">
                    <div class="">
                      <a class="btn btn-success" style="color: white;font-size: 16px;" href="<?php echo site_url();?>/medidores/editar/<?php echo $p->id_med;?>" title="Edit Cinema">
                        Editar
                      </a>
                      &nbsp;&nbsp;&nbsp;&nbsp;
                      <a class="btn btn-danger" style="color:red;font-size: 16px; color:white"  href="<?php echo site_url();?>/medidores/eliminar/<?php echo $p->id_med;?>" title="Delete ">
                        Eliminar
                      </a>
                    </div>
                  </td>
                </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
        <?php else: ?>
          <h3>No taxes in the Database</h3>
        <?php endif; ?>
      </div>
</div>




<script type="text/javascript">
new DataTable('#tblProd', {
  layout: {
      topStart: {
          buttons: [
              {
                  extend: 'pdf',
                  text: 'Reporte PDF',
              },
              {
                  extend: 'print',
                  text: 'Reporte Imprimir',
              },
              {
                  extend: 'excel',
                  text: 'Reporte Excel',
              },
              {
                  extend: 'csv', // Cambiar a 'csv' para generar un archivo CSV
                  text: 'Reporte CSV', // Cambiar el texto del botón
              }
          ]
      }
  }
});
</script>
<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>

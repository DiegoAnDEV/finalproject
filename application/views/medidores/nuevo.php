<div class="content">
  <form class="form-control col-md-8" action="<?php echo site_url('medidores/guardar');?>" method="post" role="form" enctype="multipart/form-data">
    <div class="row">
      <div class="col-md-6">
        <div class="form-group">
          <label for="fk_id_rut">Elija la Ruta</label>
          <select class="form-select" name="fk_id_rut" id="fk_id_rut" required>
            <?php foreach ($medido as $opcionTemporal): ?>
              <option value="<?php echo $opcionTemporal->id_rut?>"> <?php echo $opcionTemporal->nombre_rut?> </option>
            <?php endforeach; ?>
          </select>
        </div>
        <div class="form-group">
          <label for="fk_id_rut">Elija la Tarifa</label>
          <select class="form-select" name="fk_id_tar" id="fk_id_tar" required>
            <?php foreach ($tarifa as $opcionTemporal): ?>
              <option value="<?php echo $opcionTemporal->id_tar?>"> <?php echo $opcionTemporal->nombre_tar?> </option>
            <?php endforeach; ?>
          </select>
        </div>
        <div class="form-group">
          <label for="numero_med">Numero de Medidor:</label>
          <input class="form-control" type="text" name="numero_med" id="numero_med" required>
        </div>
        <div class="form-group">
          <label for="serie_med">Serie:</label>
          <input class="form-control" type="text" name="serie_med" id="serie_med" required>
        </div>
        <div class="form-group">
          <label for="marca_med">Marca:</label>
          <input class="form-control" type="text" name="marca_med" id="marca_med" required>
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
          <label for="observacion_med">Observaciones:</label>
          <input class="form-control" type="text" name="observacion_med" id="observacion_med" required>
        </div>
        <div class="form-group">
          <label for="estado_rut">Estado:</label>
          <select class="form-control" name="estado_rut" id="estado_rut" required>
            <option value="ACTIVO">ACTIVO</option>
            <option value="INACTIVO">INACTIVO</option>
          </select>
        </div>
        <div class="form-group">
          <label for="foto_med">Foto:</label>
          <input class="form-control" type="text" name="foto_med" id="foto_med">
        </div>
        <div class="form-group">
          <label for="lectura_inicial_med">Lectura Inicial:</label>
          <input class="form-control" type="text" name="lectura_inicial_med" id="lectura_inicial_med" required>
        </div>
        <div class="form-group">
          <label for="creacion_med">Creacion:</label>
          <input class="form-control" type="text" name="creacion_med" id="creacion_med" readonly required>
        </div>
        <div class="form-group">
          <label for="actualizacion_med">Actualizacion:</label>
          <input class="form-control" type="text" name="actualizacion_med" id="actualizacion_med" readonly required>
        </div>
      </div>
    </div>
    <div class="row mt-3">
      <div class="col-md-6">
        <button type="submit" class="btn btn-info form-control">Guardar</button>
      </div>
      <div class="col-md-6">
        <a href="<?php echo site_url()?>/medidores/index" type="submit" class="btn btn-danger form-control  text-dark">Cancelar</a>
      </div>
    </div>
  </form>
</div>

<script>
  // Obtener la fecha y hora actual
  var currentDate = new Date();

  // Formatear la fecha y hora actual
  var formattedDateTime = currentDate.toISOString().slice(0, 19).replace('T', ' '); // Formato: YYYY-MM-DD HH:MM:SS

  // Insertar la fecha y hora actual en los campos de creación y actualización
  document.getElementById('creacion_med').value = formattedDateTime;
  document.getElementById('actualizacion_med').value = formattedDateTime;
</script>

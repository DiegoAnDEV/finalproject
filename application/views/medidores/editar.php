<div class="content">
  <form class="form-control col-md-8" action="<?php echo site_url('medidores/procesarActualizacion');?>" method="post" role="form" enctype="multipart/form-data">
    <div class="col-md-3" style="padding:1rem;">
      <label for="">ID Ruta:</label>
      <input class="form-control" type="text" name="id_med" id="id_med" value="<?php echo $medidorEdit->id_med?>" hidden>
      <input class="form-control" type="text" name="fk_id_rut" id="fk_id_rut" value="<?php echo $medidorEdit->fk_id_rut?>">
    </div>
    <div class="col-md-3" style="padding:1rem;">
      <label for="">ID Tarifa:</label>
      <input class="form-control" type="text" name="fk_id_tar" id="fk_id_tar" value="<?php echo $medidorEdit->fk_id_tar?>">
    </div>
    <div class="col-md-3" style="padding:1rem;">
      <label for="">Numero de Medidor:</label>
      <input class="form-control" type="text" name="numero_med" id="numero_med" value="<?php echo $medidorEdit->numero_med?>">
    </div>
    <div class="col-md-3" style="padding:1rem;">
      <label for="">Serie:</label>
      <input class="form-control" type="text" name="serie_med" id="serie_med" value="<?php echo $medidorEdit->serie_med?>">
    </div>
    <div class="col-md-3" style="padding:1rem;">
      <label for="">Marca Medidor:</label>
      <input class="form-control" type="text" name="marca_med" id="marca_med" value="<?php echo $medidorEdit->marca_med?>">
    </div>
    <div class="col-md-3" style="padding:1rem;">
      <label for="">Observaciones:</label>
      <input class="form-control" type="text" name="observacion_med" id="observacion_med" value="<?php echo $medidorEdit->observacion_med?>">
    </div>
    <div class="col-md-3" style="padding:1rem;">
      <label for="">Estado:</label>
      <select class="form-control" name="estado_rut" id="estado_rut" value="<?php echo $medidorEdit->estado_rut?>">
        <option value="ACTIVO">ACTIVO</option>
        <option value="INACTIVO">INACTIVO</option>
      </select>
    </div>
    <div class="col-md-3" style="padding:1rem;">
      <label for="">Foto:</label>
      <input class="form-control" type="text" name="foto_med" id="foto_med" value="<?php echo $medidorEdit->foto_med?>">
    </div>
    <div class="col-md-3" style="padding:1rem;">
      <label for="">foto_med:</label>
      <input class="form-control" type="text" name="foto_med" id="foto_med" value="<?php echo $medidorEdit->foto_med?>">
    </div>
    <div class="col-md-3" style="padding:1rem;">
      <label for="">Creacion:</label>
      <input class="form-control" type="text" name="creacion_med" id="creacion_med" value="<?php echo $medidorEdit->creacion_med?>" readonly>
    </div>
    <div class="col-md-3" style="padding:1rem;">
      <label for="">Actualizacion:</label>
      <input class="form-control" type="text" name="actualizacion_med" id="actualizacion_med" value="<?php echo $medidorEdit->actualizacion_med?>" readonly>
    </div>
    <div class="col-4 mx-auto">
      <button type="submit" class="btn btn-info form-control">SAVE</button>
    </div>
    <div class="col-4 mx-auto">
      <a href="<?php echo site_url()?>/medidores/index" type="submit" class="btn btn-danger form-control">CANCEL</a>

    </div>
  </form>
</div>
<script>
  // Obtener la fecha y hora actual
  var currentDate = new Date();

  // Formatear la fecha y hora actual
  var formattedDateTime = currentDate.toISOString().slice(0, 19).replace('T', ' '); // Formato: YYYY-MM-DD HH:MM:SS

  // Insertar la fecha y hora actual en los campos de creación y actualización
  document.getElementById('creacion_med').value = formattedDateTime;
  document.getElementById('actualizacion_med').value = formattedDateTime;
</script>

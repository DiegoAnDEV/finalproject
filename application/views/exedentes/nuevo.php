<div class="content">
  <form class="form-control col-md-8" action="<?php echo site_url('Exedentes/guardar');?>" method="post" role="form" enctype="multipart/form-data">
    <div class="row">
      <div class="col-md-4" style="padding:1rem;">
        <div class="form-group">
          <label for="limite_minimo_ex">Limite Minimo:</label>
          <input class="form-control" type="text" name="limite_minimo_ex" id="limite_minimo_ex" required>
        </div>
      </div>
      <div class="col-md-4" style="padding:1rem;">
        <div class="form-group">
          <label for="limite_maximo_ex">Limite Maximo:</label>
          <input class="form-control" type="text" name="limite_maximo_ex" id="limite_maximo_ex" required>
        </div>
      </div>
      <div class="col-md-4" style="padding:1rem;">
        <div class="form-group">
          <label for="tarifa_ex">Tarifa:</label>
          <input class="form-control" type="text" name="tarifa_ex" id="tarifa_ex" required>
        </div>
      </div>
    </div>

    <!-- Ocultar los campos de id_tar, fecha_creacion_ex y fecha_actualizacion_ex -->
    <input type="hidden" name="id_tar" id="id_tar" value="1">
    <input type="hidden" name="fecha_creacion_ex" id="fecha_creacion_ex">
    <input type="hidden" name="fecha_actualizacion_ex" id="fecha_actualizacion_ex">
    <!-- Fin de los campos ocultos -->

    <div class="row mt-3">
      <div class="col-md-4 mx-auto">
        <button type="submit" class="btn btn-info form-control">Guardar</button>
      </div>
      <div class="col-md-4 mx-auto">
        <a href="<?php echo site_url()?>/exedentes/index" type="submit" class="btn btn-danger form-control text-dark">CANCELAR</a>
      </div>
    </div>
  </form>
</div>

<script>
  // Obtener la fecha y hora actual
  var currentDate = new Date();
  var currentDateTime = currentDate.toISOString().slice(0, 19).replace('T', ' '); // Formato: YYYY-MM-DD HH:MM:SS

  // Insertar la fecha y hora actual en el campo de fecha de creación solo si está vacío
  var fechaCreacion = document.getElementById('fecha_creacion_ex');
  if (fechaCreacion.value.trim() === '') {
    fechaCreacion.value = currentDateTime;
  }

  // Insertar la fecha y hora actual en el campo de fecha de actualización
  document.getElementById('fecha_actualizacion_ex').value = currentDateTime;
</script>

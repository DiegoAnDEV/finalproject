<div class="content">
  <form class="form-control col-md-8" action="<?php echo site_url('Exedentes/procesarActualizacion');?>" method="post" role="form" enctype="multipart/form-data">
    <div class="col-md-3" style="padding:1rem;">
      <label for="">Limite Minimo:</label>
      <input class="form-control" type="text" name="id_ex" id="id_ex" value="<?php echo $exdenteEdit->id_ex?>" hidden>
      <input class="form-control" type="text" name="limite_minimo_ex" id="limite_minimo_ex" value="<?php echo $exdenteEdit->limite_minimo_ex?>">
    </div>
    <div class="col-md-3" style="padding:1rem;">
      <label for="">Limite Maximo:</label>
      <input class="form-control" type="text" name="limite_maximo_ex" id="limite_maximo_ex" value="<?php echo $exdenteEdit->limite_maximo_ex?>">
    </div>
    <div class="col-md-3" style="padding:1rem;">
      <label for="">Tarifa:</label>
      <input class="form-control" type="text" name="tarifa_ex" id="tarifa_ex" value="<?php echo $exdenteEdit->tarifa_ex?>">
    </div>
    <div class="col-md-3" style="padding:1rem;">
      <label for="fecha_creacion_ex">Fecha de Creación:</label>
      <input class="form-control" type="text" name="fecha_creacion_ex" id="fecha_creacion_ex" value="<?php echo $exdenteEdit->fecha_creacion_ex?>" readonly>
    </div>
    <div class="col-md-3" style="padding:1rem;">
      <label for="fecha_actualizacion_ex">Fecha de Actualización:</label>
      <input class="form-control" type="text" name="fecha_actualizacion_ex" id="fecha_actualizacion_ex" readonly>
    </div>
    <div class="col-4 mx-auto">
      <button type="submit" class="btn btn-info form-control">SAVE</button>
    </div>
    <div class="col-4 mx-auto">
      <a href="<?php echo site_url()?>/Exedentes/index" type="submit" class="btn btn-danger form-control">CANCEL</a>
    </div>
  </form>
</div>

<script>
  // Obtener la fecha y hora actual
  var currentDate = new Date();
  var currentDateTime = currentDate.toISOString().slice(0, 19).replace('T', ' '); // Formato: YYYY-MM-DD HH:MM:SS

  // Insertar la fecha y hora actual en el campo de fecha de actualización
  document.getElementById('fecha_actualizacion_ex').value = currentDateTime;
</script>

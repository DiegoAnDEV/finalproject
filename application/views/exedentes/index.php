<div class="content">
  <div class="container-fluid">
    <div class="row">

      <div class="col-md-12">
        <legend>Lista de Excedentes</legend>
        <a class="btn btn-success" href="<?php echo site_url('Exedentes/nuevo');?>">Nuevo Excedente</a>
        <?php if ($cinemaList):?>
          <table class="table table-hover table-dark" id="tblProd" name="tblProd">
            <thead>
              <tr>
                <th>ID</th>
                <th>ID Tarifa</th>
                <th>Limite Minimo</th>
                <th>Limite Maximo</th>
                <th>Tarifa</th>
                <th>Fecha de Actualizacion</th>
                <th>Fecha de Creacion</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($cinemaList as $p): ?>
                <tr>
                  <td><?php echo $p->id_ex?></td>
                  <td><?php echo $p->id_tar?></td>
                  <td><?php echo $p->limite_minimo_ex?></td>
                  <td><?php echo $p->limite_maximo_ex?></td>
                  <td><?php echo $p->tarifa_ex?></td>
                  <td><?php echo $p->fecha_actualizacion_ex?></td>
                  <td><?php echo $p->fecha_creacion_ex?></td>
                  <td class="text-center">
                    <div class="">
                      <a class="btn btn-success" style="color: white;font-size: 16px;" href="<?php echo site_url();?>/Exedentes/editar/<?php echo $p->id_ex;?>" title="Edit Cinema">
                        Editar
                      </a>
                      &nbsp;&nbsp;&nbsp;&nbsp;
                      <a class="btn btn-danger" style="color:red;font-size: 16px; color:white"  href="<?php echo site_url();?>/Exedentes/eliminar/<?php echo $p->id_ex;?>" title="Delete ">
                        Eliminar
                      </a>
                    </div>
                  </td>
                </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
        <?php else: ?>
          <h3>No taxes in the Database</h3>
        <?php endif; ?>
      </div>
</div>

<script type="text/javascript">
new DataTable('#tblProd', {
  layout: {
      topStart: {
          buttons: [
              {
                  extend: 'pdf',
                  text: 'Reporte PDF',
              },
              {
                  extend: 'print',
                  text: 'Reporte Imprimir',
              },
              {
                  extend: 'excel',
                  text: 'Reporte Excel',
              },
              {
                  extend: 'csv', // Cambiar a 'csv' para generar un archivo CSV
                  text: 'Reporte CSV', // Cambiar el texto del botón
              }
          ]
      }
  }
});
</script>
<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>

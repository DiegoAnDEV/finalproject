<div class="content">
  <div class="container-fluid">
    <div class="row">

      <form class="form-control " id="form_nuevo" action="<?php echo site_url('Consumos/procesarActualizacion');?>" method="post" enctype="multipart/form-data">
        <br>
        <h2 class="text-center">NUEVO CONSUMO</h2>

        <br>
          <div class="row">

        <div class="col-md-6">
              <input type="text" class="form-control" name="id_consumo" id="id_consumo" value="<?php echo $consumoEditar->id_consumo?>" hidden>
          <label for="anio_consumo">AÑO:</label>
          <br>
          <input type="number" class="form-control" name="anio_consumo" id="anio_consumo" placeholder="Ingrese año" value="<?php echo $consumoEditar->anio_consumo?>"  required>
          <span id="error_anio" style="color: red;"></span>
      </div>

          <div class="col-md-6">
            <label for="">ESTADO:</label>
            <br>
            <select class="form-control"  name="estado_consumo" id="estado_consumo" value="<?php echo $consumoEditar->estado_consumo?>"  required>
              <option value="CERRADO">CERRADO</option>
              <option value="ABIERTO">ABIERTO</option>
            </select>
          </div>

        </div>
        <br>
        <div class="row">
            <div class="col-md-6">
              <label for="">MES:</label>
              <select class="form-control"  name="mes_consumo" id="mes_consumo" value="<?php echo $consumoEditar->mes_consumo?>"  required>
                <option value="ENERO">ENERO</option>
                <option value="FEBRERO">FEBRERO</option>
                <option value="MARZO">MARZO</option>
                <option value="ABRIL">ABRIL</option>
                <option value="MAYO">MAYO</option>
                <option value="JUNIO">JUNIO</option>
                <option value="JULIO">JULIO</option>
                <option value="AGOSTO">AGOSTO</option>
                <option value="SEPTIEMBRE">SEPTIEMBRE</option>
                <option value="OCTUBRE">OCTUBRE</option>
                <option value="NOVIEMBRE">NOVIEMBRE</option>
                <option value="DICIEMBRE">DICIEMBRE</option>
              </select>
            </div>
            <div class="col-md-6">
              <label for="">NUMERO DE MES DEL CONSUMO:</label>
              <select class="form-control"  name="numero_mes_consumo" id="numero_mes_consumo" value="<?php echo $consumoEditar->numero_mes_consumo?>" required>
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
                <option value="6">6</option>
                <option value="7">7</option>
                <option value="8">8</option>
                <option value="9">9</option>
                <option value="10">10</option>
                <option value="11">11</option>
                <option value="12">12</option>
              </select>
            </div>
        </div>
        <br>
        <div class="row">
          <div class="col-md-6">
            <label for="fecha_vencimiento_consumo">FECHA DE VENCIMIENTO:</label>
            <br>
            <input type="date" class="form-control" name="fecha_vencimiento_consumo" id="fecha_vencimiento_consumo" placeholder="Ingrese fecha" value="<?php echo $consumoEditar->fecha_vencimiento_consumo?>"  required>
            <span id="error_fecha_vencimiento" style="color: red;"></span>
        </div>

        </div>
        <br>

        <div class="row">
          <button type="submit" class="btn btn-info col-md-5" style="margin:1rem">GUARDAR</button>
          <a href="<?php echo site_url()?>/Consumos/index" class="btn btn-danger col-md-5" style="margin:1rem" >CANCELAR</a>
        </div>
      </form>
    </div>
  </div>


  <script>
      document.getElementById('anio_consumo').addEventListener('input', function() {
          var anio = parseInt(this.value);
          var errorMensaje = document.getElementById('error_anio');
          if (isNaN(anio)) {
              errorMensaje.textContent = "Por favor ingrese un número válido.";
          } else if (anio < 2023 || anio > 2025) {
              errorMensaje.textContent = "Por favor ingrese una fecha de vencimiento entre 2023 y 2025.";
          } else {
              errorMensaje.textContent = "";
          }
      });
  </script>
  <script>
      document.getElementById('fecha_vencimiento_consumo').addEventListener('input', function() {
          var fecha = new Date(this.value);
          var errorMensaje = document.getElementById('error_fecha_vencimiento');
          if (isNaN(fecha.getTime())) {
              errorMensaje.textContent = "Por favor ingrese una fecha válida.";
          } else {
              var anio = fecha.getFullYear();
              if (anio < 2023 || anio > 2025) {
                  errorMensaje.textContent = "Por favor ingrese una fecha de vencimiento entre 2023 y 2025.";
              } else {
                  errorMensaje.textContent = "";
              }
          }
      });
  </script>

</div>

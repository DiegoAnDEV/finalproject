<div class="content">
  <div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
          <legend>LISTA DE CONSUMOS</legend>
          <br>
          <div class="row">
            <div class="col-md-1 ">
              <a class="btn btn-secondary"   href="<?php echo site_url("Consumos/nuevo");?>">NUEVO</a>
            </div>
          </div>
          <br>
          <?php if($listCon): ?>
            <table id="tconsumo" class="table table-striped">
              <thead>
                <tr class="table table-primary">
                  <td>ID</td>
                  <td>AÑO</td>
                  <td>ESTADO</td>
                  <td>MES</td>
                  <td>MES DEL CONSUMO</td>
                  <td>VENCIMIENTO</td>
                  <td >ACCIONES</td>
                </tr>
              </thead>
              <tbody>
              <?php foreach ($listCon as $p):?>
                <tr>
                  <td><?php echo $p->id_consumo?></td>
                  <td><?php echo $p->anio_consumo?></td>
                  <td><?php echo $p->estado_consumo?></td>
                  <td><?php echo $p->mes_consumo?></td>
                  <td><?php echo $p->numero_mes_consumo?></td>
                  <td><?php echo $p->fecha_vencimiento_consumo?></td>
                  <td>
                    <a class="btn btn-info" style="margin:0.1rem"  href="<?php echo site_url("/Consumos/editar/$p->id_consumo");?>">EDITAR</a>
                    <a class="btn btn-danger" style="margin:0.1rem" href="<?php echo site_url("/Consumos/eliminar/$p->id_consumo");?>" >ELIMINAR</a>
                  </td>
                </tr>
              <?php endforeach; ?>
              </tbody>
            </table>
          <?php else: ?>
            <h3>No Asistencias in database</h3>
          <?php endif; ?>
        </div>

      </div>

      <script type="text/javascript">
      new DataTable('#tconsumo', {
        layout: {
            topStart: {
                buttons: [
                    {
                        extend: 'pdf',
                        text: 'Reporte PDF',
                    },
                    {
                        extend: 'print',
                        text: 'Reporte Imprimir',
                    },
                    {
                        extend: 'excel',
                        text: 'Reporte Excel',
                    },
                    {
                        extend: 'csv', // Cambiar a 'csv' para generar un archivo CSV
                        text: 'Reporte CSV', // Cambiar el texto del botón
                    }
                ]
            }
        }
      });
      </script>


    </div>
  </div>

</div>
